import { createMuiTheme } from '@material-ui/core/styles'
import { blueGrey, grey } from '@material-ui/core/colors'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#424242',
      contrastText: '#fff',
    },
    secondary: {
      main: '#ff5722',
      dark: '#c41c00',
      contrastText: '#fff',
    },
    extraColor: blueGrey,
    textColor: grey,
  },
  typography: {
    useNextVariants: true,
  },
})
