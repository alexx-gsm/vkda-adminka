const drawerWidth = 240

export default theme => ({
  Wrap: {
    background: 'rgba(255, 255, 255, 0.9)',
    maxWidth: '960px',
    margin: 0,
  },
})
