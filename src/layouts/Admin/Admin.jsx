import React, { Fragment } from 'react'
import { Switch, Route } from 'react-router-dom'
import classNames from 'classnames'
// material-ui components
import { Grid } from '@material-ui/core'
// components
import Appbar from '../../components/Appbar'
import { MuiThemeProvider } from '@material-ui/core/styles'
import RootRoutes from '../../routes/RootRoutes'

const switchRoutes = (routes) => (
  <Switch>
    {routes.map((route) => {
      return (
        <Route
          path={route.path}
          render={() => (
            <Fragment>
              <Switch>
                {route.sidebar.map((subroute) => {
                  return (
                    <Route
                      path={subroute.path}
                      component={subroute.component}
                      key={subroute.navbarName}
                      exact={subroute.exact}
                    />
                  )
                })}
              </Switch>
            </Fragment>
          )}
          key={route.sidebarName}
          exact={route.exact}
        />
      )
    })}
  </Switch>
)

const Admin = ({ theme, classes }) => {
  return (
    <MuiThemeProvider theme={theme}>
      <div className={classes._Root}>
        <Appbar />

        <Grid
          container
          direction='column'
          wrap='nowrap'
          className={classes._Main}
        >
          <div className={classes._ToolBar} />
          <Grid className={classNames(classes._Wrap, classes.Wrap)}>
            {switchRoutes(RootRoutes)}
          </Grid>
        </Grid>
      </div>
    </MuiThemeProvider>
  )
}

export default Admin
