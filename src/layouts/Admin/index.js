import { compose, withProps } from 'recompose'
// hocs
import { withStyles } from '@material-ui/core/styles'
import CommonStyles from '../../styles/CommonStyles'
import theme from './theme'
// styles
import styles from './style'
// component
import Admin from './Admin'

export default compose(
  withProps({ theme }),
  withStyles(() => ({ ...CommonStyles(theme), ...styles(theme) }), {
    withTheme: true,
  })
)(Admin)
