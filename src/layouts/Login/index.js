import { compose, withState, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { login } from '../../redux/modules/user'
import { withStyles } from '@material-ui/core/styles'
import Login from './Login'
// styles
import styles from './styles'

export default compose(
  connect(
    null,
    { login }
  ),
  withRouter,
  withState('userData', 'setUserData', {}),
  withState('isPasswordVisible', 'setPasswordVisible', false),
  withHandlers({
    onChange: ({ userData, setUserData }) => event =>
      setUserData({
        ...userData,
        [event.target.name]: event.target.value,
      }),
    onShowPassword: ({ isPasswordVisible, setPasswordVisible }) => () => {
      setPasswordVisible(!isPasswordVisible)
    },
    onLogin: ({ userData, login, history }) => () =>
      login(userData, () => history.push('/')),
  }),
  withStyles(styles, { withTheme: true })
)(Login)
