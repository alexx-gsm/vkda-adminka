import React from 'react'
// material-ui components
import { FormControl, IconButton, Typography } from '@material-ui/core'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
// @material-ui/icons
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import AccountCircle from '@material-ui/icons/AccountCircle'
import isEmpty from '../../helpers/is-empty'

const Login = ({
  classes,
  userData,
  onChange,
  onLogin,
  isPasswordVisible,
  onShowPassword,
}) => {
  const { email, password } = userData
  return (
    <Grid
      container
      className={classes.root}
      alignItems="flex-start"
      justify="center"
    >
      <Paper className={classes.container} elevation={1}>
        <form className={classes.form} noValidate autoComplete="off">
          <Typography variant="h5">Вход</Typography>
          <Grid container justify="center">
            <AccountCircle className={classes.avatar} />
          </Grid>

          <TextField
            required
            id="email"
            label="E-mail"
            // className={classes.textField}
            margin="normal"
            value={!isEmpty(email) ? email : ''}
            name="email"
            onChange={onChange}
          />

          <FormControl className={classes.textField}>
            <InputLabel htmlFor="adornment-password" required>
              Password
            </InputLabel>
            <Input
              required
              id="adornment-password"
              type={isPasswordVisible ? 'text' : 'password'}
              value={!isEmpty(password) ? password : ''}
              name="password"
              onChange={onChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={onShowPassword}
                  >
                    {isPasswordVisible ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={onLogin}
          >
            Войти
          </Button>
        </form>
      </Paper>
    </Grid>
  )
}

export default Login
