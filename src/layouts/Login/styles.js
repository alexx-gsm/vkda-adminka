export default (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    marginTop: theme.spacing(3),
    background: 'rgba(0,0,0,0.4)',
    [theme.breakpoints.down('xs')]: {
      minWidth: 0,
      marginTop: 0,
    },
  },
  avatar: {
    color: '#ddd',
    fontSize: '80px',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    padding: '30px',
    background: '#fbfbfb',
    [theme.breakpoints.down('xs')]: {
      width: '100vw',
      height: '100vh',
      boxSizing: 'border-box',
    },
  },
  GridActionButton: {
    [theme.breakpoints.down('xs')]: {
      marginTop: 'auto',
    },
  },
  textField: {
    flexBasis: 'auto',
  },
  margin: {
    margin: theme.spacing(1),
  },
  button: {
    marginTop: theme.spacing(3),
  },
  ButtonPrimary: {
    fontSize: '0.75rem',
    marginTop: theme.spacing(3),
    color: theme.palette.grey[500],
  },
})
