import axios from 'axios'
import jwt_decode from 'jwt-decode'
import store from '../redux/store'
import { USER_AUTH_SET, logout } from '../redux/modules/user'

export const setAuthToken = (token) => {
  if (token) {
    axios.defaults.headers.common['Authorization'] = token
  } else {
    delete axios.defaults.headers.common['Authorization']
  }
}

export const checkAuthToken = () => {
  if (localStorage.jwtToken) {
    setAuthToken(localStorage.jwtToken)
    const decoded = jwt_decode(localStorage.jwtToken)
    store.dispatch({
      type: USER_AUTH_SET,
      payload: { authUser: decoded },
    })

    const currentTime = Date.now() / 1000
    if (decoded.exp < currentTime) {
      store.dispatch(logout(() => (window.location.href = '/login')))
    }
  }
}
