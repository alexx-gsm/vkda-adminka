import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { getAllVendors } from '../../../../redux/modules/vendor'
// styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'

import VendorList from './VendorList'

export default compose(
  connect(
    ({ vendorStore }) => {
      const { vendors, loading, error } = vendorStore
      return {
        vendors,
        loading,
        error,
      }
    },
    { getAllVendors }
  ),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
  }),
  lifecycle({
    componentDidMount() {
      this.props.getAllVendors()
    },
    componentWillUnmount() {},
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(VendorList)
