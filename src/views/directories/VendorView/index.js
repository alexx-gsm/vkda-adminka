import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const VendorList = lazy(() => import('./VendorList'))
const VendorEdit = lazy(() => import('./VendorEdit'))

const basePath = '/directories/vendors'

const VendorView = () => {
  return (
    <Suspense fallback={<LinearIndeterminate />}>
      <Switch>
        <Route
          exact
          path={`${basePath}`}
          render={() => <VendorList basePath={basePath} theme={theme} />}
        />
        <Route
          path={`${basePath}/edit/:id?`}
          render={() => <VendorEdit basePath={basePath} theme={theme} />}
        />
      </Switch>
    </Suspense>
  )
}

export default VendorView
