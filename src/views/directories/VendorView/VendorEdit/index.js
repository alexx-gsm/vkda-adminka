import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getOneVendor,
  setVendor,
  saveVendor,
  deleteVendor,
} from '../../../../redux/modules/vendor'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'

import VendorEdit from './VendorEdit'

export default compose(
  connect(
    ({ vendorStore }) => {
      const { vendor, loading, error } = vendorStore

      return {
        vendor,
        loading,
        error,
      }
    },
    { getOneVendor, setVendor, saveVendor, deleteVendor }
  ),
  withRouter,
  withState('dialogIsVisible', 'setDialogIsVisible', false),
  withHandlers({
    onChange: ({ vendor, setVendor }) => (index) => (event) => {
      setVendor({
        ...vendor,
        [index]: event.target.value,
      })
    },
    onBack: ({ basePath, history }) => () => history.push(basePath),
    onSave: ({ vendor, saveVendor, basePath, history }) => () => {
      saveVendor(vendor, () => history.push(basePath))
    },
    onDelete: ({ setDialogIsVisible }) => () => {
      setDialogIsVisible(true)
    },
    cancelDialog: ({ setDialogIsVisible, setItem }) => () => {
      setDialogIsVisible(false)
    },
    confirmDialog: ({
      vendor,
      setDialogIsVisible,
      deleteVendor,
      basePath,
      history,
    }) => () => {
      setDialogIsVisible(false)
      deleteVendor(vendor._id, () => history.push(basePath))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneVendor(id)
      }
    },
    componentWillUnmount() {
      this.props.setVendor({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(VendorEdit)
