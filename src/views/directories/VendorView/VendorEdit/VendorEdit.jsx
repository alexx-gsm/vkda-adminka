import React, { Fragment } from 'react'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import AlertDialog from '../../../../components/AlertDialog'
// Material UI
import { Grid, Typography, TextField, Button } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const VendorEdit = ({
  vendor,
  loading,
  error,
  onChange,
  onBack,
  onSave,
  onDelete,
  dialogIsVisible,
  cancelDialog,
  confirmDialog,
  basePath,
  theme,
  classes,
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <Grid className={classes._Row}>
            <TextField
              fullWidth
              id='title'
              label='Название'
              value={isEmpty(vendor.title) ? '' : vendor.title}
              onChange={onChange('title')}
              margin='normal'
              error={Boolean(error.title)}
              helperText={error.title ? error.title : null}
            />
            <TextField
              fullWidth
              id='person'
              label='Контактное лицо | ФИО'
              value={isEmpty(vendor.person) ? '' : vendor.person}
              onChange={onChange('person')}
              margin='normal'
            />
            <TextField
              fullWidth
              id='phone'
              label='Контактное лицо | телефон'
              value={isEmpty(vendor.phone) ? '' : vendor.phone}
              onChange={onChange('phone')}
              margin='normal'
            />
            <TextField
              fullWidth
              id='address'
              label='Адрес'
              value={isEmpty(vendor.address) ? '' : vendor.address}
              onChange={onChange('address')}
              margin='normal'
            />
            <TextField
              fullWidth
              id='email'
              label='e-mail'
              value={isEmpty(vendor.email) ? '' : vendor.email}
              onChange={onChange('email')}
              margin='normal'
              error={Boolean(error.email)}
              helperText={error.email ? error.email : null}
            />
            <TextField
              fullWidth
              id='comment'
              label='Комментарий'
              value={isEmpty(vendor.comment) ? '' : vendor.comment}
              onChange={onChange('comment')}
              margin='normal'
            />
          </Grid>

          <Grid className={classes._Row}>
            <Grid
              container
              spacing={1}
              justify='space-between'
              className={classes._MT2}
            >
              <Grid item>
                <Button
                  variant='text'
                  onClick={onDelete}
                  disabled={!vendor._id}
                  color='secondary'
                >
                  Удалить
                </Button>
              </Grid>
              <Grid item className={classes._MLAuto}>
                <Button
                  onClick={onBack}
                  aria-label='Back'
                  variant='text'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
      <AlertDialog
        title='Удалить?'
        text={vendor.title}
        isVisible={dialogIsVisible}
        onCancel={cancelDialog}
        onConfirm={confirmDialog}
      />
    </MuiThemeProvider>
  )
}

export default VendorEdit
