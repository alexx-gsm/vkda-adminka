import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const WarehouseList = lazy(() => import('./WarehouseList'))
const WarehouseEdit = lazy(() => import('./WarehouseEdit'))

const basePath = '/directories/warehouses'

const WarehouseView = () => {
  return (
    <Suspense fallback={<LinearIndeterminate />}>
      <Switch>
        <Route
          exact
          path={`${basePath}`}
          render={() => <WarehouseList basePath={basePath} theme={theme} />}
        />
        <Route
          path={`${basePath}/edit/:id?`}
          render={() => <WarehouseEdit basePath={basePath} theme={theme} />}
        />
      </Switch>
    </Suspense>
  )
}

export default WarehouseView
