import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getOneWarehouse,
  setWarehouse,
  saveWarehouse,
  deleteWarehouse,
} from '../../../../redux/modules/warehouse'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'

import WarehouseEdit from './WarehouseEdit'

export default compose(
  connect(
    ({ warehouseStore }) => {
      const { warehouse, loading, error } = warehouseStore

      return {
        warehouse,
        loading,
        error,
      }
    },
    { getOneWarehouse, setWarehouse, saveWarehouse, deleteWarehouse }
  ),
  withRouter,
  withState('dialogIsVisible', 'setDialogIsVisible', false),
  withHandlers({
    onChange: ({ warehouse, setWarehouse }) => (index) => (event) => {
      setWarehouse({
        ...warehouse,
        [index]: event.target.value,
      })
    },
    onBack: ({ basePath, history }) => () => history.push(basePath),
    onSave: ({ warehouse, saveWarehouse, basePath, history }) => () => {
      saveWarehouse(warehouse, () => history.push(basePath))
    },
    onDelete: ({ setDialogIsVisible }) => () => {
      setDialogIsVisible(true)
    },
    cancelDialog: ({ setDialogIsVisible, setItem }) => () => {
      setDialogIsVisible(false)
    },
    confirmDialog: ({
      warehouse,
      setDialogIsVisible,
      deleteWarehouse,
      basePath,
      history,
    }) => () => {
      setDialogIsVisible(false)
      deleteWarehouse(warehouse._id, () => history.push(basePath))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneWarehouse(id)
      }
    },
    componentWillUnmount() {
      this.props.setWarehouse({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(WarehouseEdit)
