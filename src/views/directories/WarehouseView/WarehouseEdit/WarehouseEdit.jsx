import React, { Fragment } from 'react'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import AlertDialog from '../../../../components/AlertDialog'
// Material UI
import { Grid, Typography, TextField, Button } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const WarehouseEdit = ({
  warehouse,
  loading,
  error,
  onChange,
  onBack,
  onSave,
  onDelete,
  dialogIsVisible,
  cancelDialog,
  confirmDialog,
  basePath,
  theme,
  classes,
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <Grid className={classes._Row}>
            <TextField
              fullWidth
              id='title'
              label='Название'
              value={isEmpty(warehouse.title) ? '' : warehouse.title}
              onChange={onChange('title')}
              margin='normal'
              className={classes.textName}
              error={Boolean(error.title)}
              helperText={error.title ? error.title : null}
            />
          </Grid>
          <Grid className={classes._Row}>
            <Grid
              container
              className={classes._MT2}
              spacing={1}
              justify='space-between'
            >
              <Grid item>
                <Button
                  variant='text'
                  onClick={onDelete}
                  disabled={!warehouse._id}
                  color='secondary'
                >
                  Удалить
                </Button>
              </Grid>
              <Grid item className={classes._MLAuto}>
                <Button
                  onClick={onBack}
                  aria-label='Back'
                  variant='text'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>

          <AlertDialog
            title='Удалить?'
            text={warehouse.title}
            isVisible={dialogIsVisible}
            onCancel={cancelDialog}
            onConfirm={confirmDialog}
          />
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
    </MuiThemeProvider>
  )
}

export default WarehouseEdit
