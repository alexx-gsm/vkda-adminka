import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { getAllWarehouses } from '../../../../redux/modules/warehouse'
// styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'

import WarehouseList from './WarehouseList'

export default compose(
  connect(
    ({ warehouseStore }) => {
      const { warehouses, loading, error } = warehouseStore
      return {
        warehouses,
        loading,
        error,
      }
    },
    { getAllWarehouses }
  ),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => id => () =>
      history.push(`${basePath}/edit/${id}`),
  }),
  lifecycle({
    componentDidMount() {
      this.props.getAllWarehouses()
    },
    componentWillUnmount() {},
  }),
  withStyles(
    theme => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(WarehouseList)
