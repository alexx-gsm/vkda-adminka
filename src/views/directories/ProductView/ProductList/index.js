import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { getAllProducts } from '../../../../redux/modules/product'
// styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'

import ProductList from './ProductList'

export default compose(
  connect(
    ({ productStore }) => {
      const { products, loading, error } = productStore
      return {
        products,
        loading,
        error,
      }
    },
    { getAllProducts }
  ),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
  }),
  lifecycle({
    componentDidMount() {
      this.props.getAllProducts()
    },
    componentWillUnmount() {},
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(ProductList)
