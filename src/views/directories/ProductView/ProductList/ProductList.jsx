import React, { Fragment, useState } from 'react'
import { Link } from 'react-router-dom'
// @material-ui components
import { Grid, Typography, Fab, List, ListItem } from '@material-ui/core'
import { TablePagination } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-icons
import AddIcon from '@material-ui/icons/Add'

function ProductList({
  products,
  handleClick,
  loading,
  basePath,
  theme,
  classes,
}) {
  const [rowsPerPage, handleChangeRowsPerPage] = useState(5)
  const [page, handleChangePage] = useState(0)

  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <List className={classes.PaperList}>
            {products
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((product) => (
                <ListItem
                  key={product._id}
                  button
                  divider
                  onClick={handleClick(product._id)}
                  classes={{
                    root: classes._ListItem,
                  }}
                >
                  <Typography variant='h6'>{product.title}</Typography>
                </ListItem>
              ))}
          </List>
          <Grid className={classes._Row}>
            <Grid
              className={classes._GridActions}
              container
              spacing={2}
              justify='space-between'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item>
                <Fab
                  aria-label='Add'
                  color='secondary'
                  size='medium'
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              <Grid item>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={products.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={(e, page) => handleChangePage(page)}
                  onChangeRowsPerPage={(e) =>
                    handleChangeRowsPerPage(e.target.value)
                  }
                  labelRowsPerPage='Кол-во на стр.'
                />
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <React.Fragment>
          <LinearIndeterminate />
        </React.Fragment>
      )}
    </MuiThemeProvider>
  )
}

export default ProductList
