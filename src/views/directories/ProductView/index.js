import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const ProductList = lazy(() => import('./ProductList'))
const ProductEdit = lazy(() => import('./ProductEdit'))

const basePath = '/directories/products'

const ProductView = () => {
  return (
    <Suspense fallback={<LinearIndeterminate />}>
      <Switch>
        <Route
          exact
          path={`${basePath}`}
          render={() => <ProductList basePath={basePath} theme={theme} />}
        />
        <Route
          path={`${basePath}/edit/:id?`}
          render={() => <ProductEdit basePath={basePath} theme={theme} />}
        />
      </Switch>
    </Suspense>
  )
}

export default ProductView
