import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getOneProduct,
  setProduct,
  saveProduct,
  deleteProduct,
} from '../../../../redux/modules/product'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'

import ProductEdit from './ProductEdit'

export default compose(
  connect(
    ({ productStore }) => {
      const { product, loading, error } = productStore

      return {
        product,
        loading,
        error,
      }
    },
    { getOneProduct, setProduct, saveProduct, deleteProduct }
  ),
  withRouter,
  withState('dialogIsVisible', 'setDialogIsVisible', false),
  withHandlers({
    onChange: ({ product, setProduct }) => (index) => (event) => {
      setProduct({
        ...product,
        [index]: event.target.value,
      })
    },
    onBack: ({ basePath, history }) => () => history.push(basePath),
    onSave: ({ product, saveProduct, basePath, history }) => () => {
      saveProduct(product, () => history.push(basePath))
    },
    onDelete: ({ setDialogIsVisible }) => () => {
      setDialogIsVisible(true)
    },
    cancelDialog: ({ setDialogIsVisible, setItem }) => () => {
      setDialogIsVisible(false)
    },
    confirmDialog: ({
      product,
      setDialogIsVisible,
      deleteProduct,
      basePath,
      history,
    }) => () => {
      setDialogIsVisible(false)
      deleteProduct(product._id, () => history.push(basePath))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneProduct(id)
      }
    },
    componentWillUnmount() {
      this.props.setProduct({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(ProductEdit)
