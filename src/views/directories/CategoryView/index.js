import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const CategoryList = lazy(() => import('./CategoryList'))
const CategoryEdit = lazy(() => import('./CategoryEdit'))

const basePath = '/directories/categories'

const UserView = () => {
  return (
    <Suspense fallback={<LinearIndeterminate />}>
      <Switch>
        <Route
          exact
          path={`${basePath}`}
          render={() => <CategoryList basePath={basePath} theme={theme} />}
        />
        <Route
          path={`${basePath}/edit/:id?`}
          render={() => <CategoryEdit basePath={basePath} theme={theme} />}
        />
      </Switch>
    </Suspense>
  )
}

export default UserView
