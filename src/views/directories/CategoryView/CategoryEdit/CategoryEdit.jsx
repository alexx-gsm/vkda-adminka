import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
// components
import AlertDialog from '../../../../components/AlertDialog'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// useful utils
import isEmpty from '../../../../helpers/is-empty'
import classNames from 'classnames'
// @material-ui
import { Button, Grid, Typography, TextField } from '@material-ui/core'
import { FormControl, FormGroup, FormControlLabel } from '@material-ui/core'
import { Switch, MenuItem } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'

function CategoryEdit({
  category,
  rootCategories,
  basePath,
  classes,
  error,
  loading,
  onChange,
  onCheck,
  onSelect,
  onSave,
  onDelete,
  isVisibleDelete,
  dialogCancel,
  dialogConfirm,
  theme,
}) {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Grid className={classes.Wrap}>
          <TextField
            required
            fullWidth
            id='title'
            label='Название'
            value={!isEmpty(category.title) ? category.title : ''}
            onChange={onChange('title')}
            margin='normal'
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />

          <Grid container spacing={1} alignItems='flex-end'>
            <Grid item xs={12} sm={2}>
              <FormControl component='fieldset'>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={!isEmpty(category) && category.isRoot}
                        onChange={onCheck}
                        value='isRoot'
                        disabled={isEmpty(rootCategories)}
                        margin='normal'
                      />
                    }
                    label='isRoot'
                  />
                </FormGroup>
              </FormControl>
            </Grid>
            <Grid item xs>
              <TextField
                select
                id='category'
                label='Root категория'
                value={!isEmpty(category.rootId) ? category.rootId : ''}
                onChange={onSelect}
                className={classes.Select}
                fullWidth
                margin='normal'
                disabled={category.isRoot || isEmpty(rootCategories)}
                error={Boolean(error.rootId)}
                helperText={error.rootId ? error.rootId : null}
              >
                {rootCategories.map((category) => {
                  return (
                    <MenuItem key={category._id} value={category._id}>
                      <Grid
                        container
                        alignItems='baseline'
                        className={classes.gridType}
                        direction='row'
                        wrap='wrap'
                      >
                        <Typography
                          className={classes.typoSelectInputTitle}
                          // variant='h6'
                        >
                          {category.title}
                        </Typography>
                      </Grid>
                    </MenuItem>
                  )
                })}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={2}>
              <TextField
                fullWidth
                id='sorting'
                label='Сортировка'
                value={isEmpty(category.sorting) ? '' : category.sorting}
                onChange={onChange('sorting')}
                type='number'
                placeholder='0'
                min={0}
                step={1}
                margin='normal'
              />
            </Grid>
          </Grid>

          <Grid className={classNames(classes._MT2)}>
            <Grid
              container
              className={classes._GridActions}
              spacing={1}
              justify='space-between'
            >
              <Grid item>
                <Button
                  variant='text'
                  onClick={onDelete}
                  disabled={!category._id}
                  color='secondary'
                >
                  Удалить
                </Button>
              </Grid>
              <Grid item className={classes._MLAuto}>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label='Back'
                  variant='text'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}

      <AlertDialog
        title='Удалить?'
        text={category.title}
        isVisible={isVisibleDelete}
        onCancel={dialogCancel}
        onConfirm={dialogConfirm}
      />
    </MuiThemeProvider>
  )
}

export default CategoryEdit
