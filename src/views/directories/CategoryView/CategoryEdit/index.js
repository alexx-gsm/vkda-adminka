import { connect } from 'react-redux'
import { compose, lifecycle, withHandlers, withState } from 'recompose'
import { withRouter } from 'react-router-dom'
// AC
import {
  getCategories,
  getOneCategory,
  setCategory,
  saveCategory,
  deleteCategory,
} from '../../../../redux/modules/category'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// core components
import CategoryEdit from './CategoryEdit'
import isEmpty from '../../../../helpers/is-empty'

export default compose(
  connect(
    ({ categoryStore }) => {
      const { category, categories, error, loading } = categoryStore
      const rootCategories = !isEmpty(categories)
        ? categories.filter((c) => c.isRoot)
        : []

      return {
        category,
        rootCategories,
        error,
        loading,
      }
    },
    { getCategories, getOneCategory, setCategory, saveCategory, deleteCategory }
  ),
  withRouter,
  withState('isVisibleDelete', 'setIsVisibleDelete', false),
  withHandlers({
    onChange: ({ category, setCategory }) => (name) => (event) => {
      setCategory({
        ...category,
        [name]: event.target.value,
      })
    },
    onCheck: ({ category, setCategory }) => (event) => {
      setCategory({
        ...category,
        isRoot: event.target.checked,
      })
    },
    onSelect: ({ category, setCategory }) => (event) => {
      setCategory({
        ...category,
        rootId: event.target.value,
      })
    },
    onSave: ({
      category,
      rootCategories,
      saveCategory,
      basePath,
      history,
    }) => () =>
      saveCategory({ ...category, isRoot: isEmpty(rootCategories) }, () =>
        history.push(basePath)
      ),
    onDelete: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(true),
    dialogCancel: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(false),
    dialogConfirm: ({
      category,
      deleteCategory,
      setIsVisibleDelete,
      basePath,
      history,
    }) => () => {
      deleteCategory(category._id)
      setIsVisibleDelete(false)
      history.push(basePath)
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCategories()
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneCategory(id)
      }
    },
    componentWillUnmount() {
      this.props.setCategory({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(CategoryEdit)
