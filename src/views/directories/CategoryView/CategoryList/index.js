import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import { connect } from 'react-redux'
// AC
import { getCategories } from '../../../../redux/modules/category'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// tools
import isEmpty from '../../../../helpers/is-empty'

import CategoryList from './CategoryList'

export default compose(
  connect(
    ({ categoryStore }) => {
      const { categories, loading, error } = categoryStore

      return {
        categories,
        loading,
        error,
      }
    },
    { getCategories }
  ),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCategories()
    },
    componentWillUnmount() {},
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(CategoryList)
