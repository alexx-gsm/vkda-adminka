import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
// @material-ui components
import { Grid, Typography, Fab, TablePagination } from '@material-ui/core'
import { List, ListItem, IconButton } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-icons
import IconNavBefore from '@material-ui/icons/NavigateBefore'
import IconNavNext from '@material-ui/icons/NavigateNext'
import AddIcon from '@material-ui/icons/Add'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

function CategoryList({
  categories,
  handleClick,
  loading,
  basePath,
  classes,
  theme,
}) {
  const [rowsPerPage, handleChangeRowsPerPage] = useState(5)
  const [page, handleChangePage] = useState(0)

  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <List className={classes.PaperList}>
            {categories
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item) => (
                <ListItem
                  key={item._id}
                  button
                  divider
                  onClick={handleClick(item._id)}
                  className={item.isRoot ? classes.ListRowRoot : ''}
                  classes={{
                    root: classes._ListItem,
                  }}
                >
                  <Grid container direction='column'>
                    <Grid item>
                      <Typography variant='h6'>{item.title}</Typography>
                    </Grid>
                    {!item.isRoot && (
                      <Grid container spacing={1} alignItems='baseline'>
                        <Grid item>
                          <Typography variant='caption'>
                            {
                              categories.find((c) => c._id === item.rootId)
                                .title
                            }
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant='caption'>
                            ({item.sorting})
                          </Typography>
                        </Grid>
                      </Grid>
                    )}
                  </Grid>
                </ListItem>
              ))}
          </List>
          <Grid className={classes._Row}>
            <Grid
              className={classes._GridActions}
              container
              spacing={2}
              justify='space-between'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item>
                <Fab
                  aria-label='Add'
                  color='secondary'
                  size='medium'
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              <Grid item>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={categories.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={(e, page) => handleChangePage(page)}
                  onChangeRowsPerPage={(e) =>
                    handleChangeRowsPerPage(e.target.value)
                  }
                  labelRowsPerPage='Кол-во на стр.'
                />
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
    </MuiThemeProvider>
  )
}

export default CategoryList
