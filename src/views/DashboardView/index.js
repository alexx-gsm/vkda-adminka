import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'
// theme
import theme from './theme'

const Dashboard = lazy(() => import('./Dashboard'))

const basePath = '/dashboard'

const DashboardView = () => {
  return (
    <Suspense fallback={<LinearIndeterminate />}>
      <Switch>
        <Route
          exact
          path={`${basePath}`}
          render={() => <Dashboard basePath={basePath} theme={theme} />}
        />
      </Switch>
    </Suspense>
  )
}

export default DashboardView
