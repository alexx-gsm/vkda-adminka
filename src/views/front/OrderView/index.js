import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const OrderList = lazy(() => import('./OrderList'))
const OrderEdit = lazy(() => import('./OrderEdit'))

const basePath = '/front/orders'

const OrderView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <OrderList basePath={basePath} theme={theme} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <OrderEdit basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default OrderView
