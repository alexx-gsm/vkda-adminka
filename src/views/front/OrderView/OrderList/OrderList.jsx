import React, { Fragment, useState } from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import DayPicker from '../../../../components/DayPicker'
// @material-ui components
import { Grid, Typography, Fab, TablePagination } from '@material-ui/core'
import { List, ListItem, IconButton } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-icons
import IconNavBefore from '@material-ui/icons/NavigateBefore'
import IconNavNext from '@material-ui/icons/NavigateNext'
import AddIcon from '@material-ui/icons/Add'
import NewIcon from '@material-ui/icons/FiberNew'
import RestaurantIcon from '@material-ui/icons/Restaurant'
import DeliveryIcon from '@material-ui/icons/LocalShipping'
import DoneIcon from '@material-ui/icons/Done'
import ClearIcon from '@material-ui/icons/Clear'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const getStatusIcon = (status) => {
  switch (status) {
    case 'new':
      return <NewIcon />
    case 'canteen':
      return <RestaurantIcon />
    case 'delivery':
      return <DeliveryIcon />
    case 'done':
      return <DoneIcon />
    case 'cancelled':
      return <ClearIcon />
    default:
      break
  }
}

const OrderList = ({
  orders,
  selectedDeliveryDate,
  handleDayChange,
  handleClick,
  formatter,
  loading,
  error,
  classes,
  theme,
  basePath,
}) => {
  const [rowsPerPage, handleChangeRowsPerPage] = useState(25)
  const [page, handleChangePage] = useState(0)

  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <Grid container spacing={1}>
            <Grid item>
              <DayPicker
                format='D MMMM YYYY'
                value={selectedDeliveryDate}
                onDayClick={handleDayChange}
                margin='normal'
                label='Дата доставки'
              />
            </Grid>
          </Grid>

          <List className={classes.PaperDishes}>
            {orders
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item) => (
                <ListItem
                  key={item._id}
                  button
                  divider
                  onClick={handleClick(item._id)}
                  classes={{
                    root: classes._ListItem,
                  }}
                >
                  <Grid
                    container
                    spacing={1}
                    justify='space-between'
                    alignItems='center'
                    wrap='nowrap'
                  >
                    <Grid item>{getStatusIcon(item.status)}</Grid>
                    <Grid container spacing={1} alignItems='baseline'>
                      <Grid item>
                        <Typography
                          variant='h6'
                          align='left'
                          className={classes._ListTitle}
                        >
                          {item.name}
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant='caption'
                          align='center'
                          className={classes.ListSubTitle}
                        >
                          /
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant='caption'
                          align='center'
                          className={classes.ListSubTitle}
                        >
                          {item.address}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Typography variant='h6' align='right'>
                        {item.total ? formatter.format(item.total) : ''}
                      </Typography>
                    </Grid>
                  </Grid>
                </ListItem>
              ))}
          </List>

          {/* PAGER */}
          <Grid className={classes._Row}>
            <Grid
              className={classes._GridActions}
              container
              spacing={2}
              justify='space-between'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item>
                <Fab
                  aria-label='Add'
                  color='secondary'
                  size='medium'
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              <Grid item>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={orders.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={(e, page) => handleChangePage(page)}
                  onChangeRowsPerPage={(e) =>
                    handleChangeRowsPerPage(e.target.value)
                  }
                  labelRowsPerPage='Кол-во на стр.'
                />
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
    </MuiThemeProvider>
  )
}

export default OrderList
