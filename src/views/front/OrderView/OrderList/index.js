import { withRouter } from 'react-router-dom'
import { compose, withProps, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getOrdersByDeliveryDate,
  setDeliveryDate,
} from '../../../../redux/modules/order'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// tools
import isEmpty from '../../../../helpers/is-empty'

import OrderList from './OrderList'

export default compose(
  connect(
    ({ orderStore }) => {
      const { orders, selectedDeliveryDate } = orderStore
      return {
        orders,
        selectedDeliveryDate,
      }
    },
    { getOrdersByDeliveryDate, setDeliveryDate }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withRouter,
  withHandlers({
    handleDayChange: ({ setDeliveryDate }) => (date) => setDeliveryDate(date),
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
    handleFilter: ({ setSelectedCategory }) => (e) => {
      setSelectedCategory(e.target.value)
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.getOrdersByDeliveryDate(this.props.selectedDeliveryDate)
    },
    componentDidUpdate(prevProps) {
      if (prevProps.selectedDeliveryDate !== this.props.selectedDeliveryDate) {
        this.props.getOrdersByDeliveryDate(this.props.selectedDeliveryDate)
      }
    },
    componentWillUnmount() {},
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(OrderList)
