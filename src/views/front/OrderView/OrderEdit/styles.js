export default (theme) => ({
  Form: {
    marginBottom: theme.spacing(4),
    paddingTop: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
    },
  },
  textName: {},
  inputName: {
    fontSize: '2rem',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.5rem',
    },
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  ButtonUpload: {
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center',
    },
  },
  // buttonDelete: {
  //   color: 'white',
  //   backgroundColor: red[600],
  //   '&:hover': { backgroundColor: red[700] }
  // },
  btnPrimary: {
    marginLeft: '10px',
  },
  //
  PaperLogin: {
    marginTop: '20px',
    padding: '10px 20px 20px',
    background: theme.palette.grey[50],
  },
  // img edit
  CropperWrap: {
    width: '100%',
    height: '400px',
    position: 'relative',
    marginTop: '10px',
  },

  CropperControls: {
    margin: 'auto',
    width: '50%',
    height: '80px',
    display: 'flex',
    alignItems: 'center',
  },
  Image: {
    maxWidth: '400px',
    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-start',
      margin: '0 auto',
      paddingTop: '5px',
    },
    '& img': {
      maxWidth: '100%',
    },
  },
  CropperSlider: {
    padding: '22px 0px',
  },
  Upload: {
    display: 'none',
  },
  UploadButton: {
    margin: `${theme.spacing(1)}px 0`,
  },
  UploadList: {
    maxHeight: '60vh',
    overflow: 'auto',
  },
})
