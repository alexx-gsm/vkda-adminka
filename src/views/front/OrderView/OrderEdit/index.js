import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withProps,
} from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import { getOneOrder } from '../../../../redux/modules/order'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'

import OrderEdit from './OrderEdit'

export default compose(
  connect(
    ({ orderStore }) => {
      const { order } = orderStore
      return {
        order,
      }
    },
    { getOneOrder }
  ),
  withRouter,

  withHandlers({
    onChange: ({ dish, setDish }) => (index) => (event) => {
      setDish({
        ...dish,
        [index]: event.target.value,
      })
    },
    onSave: ({
      dish,
      imgSrc,
      croppedImg,
      saveDish,
      history,
      basePath,
    }) => () => {
      saveDish(
        {
          ...dish,
          upload: croppedImg ? croppedImg : imgSrc,
          // image: '',
        },
        () => history.push(basePath)
      )
    },
    onDelete: ({ setIsVisibleDelete }) => () => {
      setIsVisibleDelete(true)
    },
    dialogCancel: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(false),
    dialogConfirm: ({ dish, deleteDish, basePath, history }) => () => {
      deleteDish(dish._id, () => history.push(basePath))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneOrder(id)
      }
    },
    componentWillUnmount() {},
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(OrderEdit)
