import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Cropper from 'react-easy-crop'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import AlertDialog from '../../../../components/AlertDialog'
// Material UI
import { Grid, Typography, TextField, Button } from '@material-ui/core'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-UI/lab
import Slider from '@material-ui/lab/Slider'
// React icons kit
import { Icon } from 'react-icons-kit'
import { rub } from 'react-icons-kit/fa/rub'
import { balanceScale } from 'react-icons-kit/fa/balanceScale'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
import { HOST } from '../../../../config'

const OrderEdit = ({
  order,
  onSave,
  onDelete,
  isVisibleDelete,
  dialogCancel,
  dialogConfirm,
  loading,
  classes,
  theme,
  basePath,
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          {/* <TopPanel title='Блюдо' /> */}

          <form
            noValidate
            className={classNames(classes._Form, classes.Form)}
            noValidate
            autoComplete='off'
          />
          {!isEmpty(order) && (
            <Fragment>
              <Typography variant='h6'>{order.name}</Typography>
              <Typography variant='caption'>{order.address}</Typography>
              <Typography variant='caption'>{order.phone}</Typography>

              <List>
                {Object.keys(order.cart).map((id, i) => (
                  <ListItem divider>
                    <ListItemText
                      primary={order.cart[id].title}
                      secondary={`${order.cart[id].count}шт.`}
                    />
                  </ListItem>
                ))}
              </List>
            </Fragment>
          )}

          <Grid className={classNames(classes._Row, classes._PB)}>
            <Grid
              container
              className={classes._GridActions}
              spacing={1}
              justify='space-between'
            >
              <Grid item>
                <Button
                  variant='text'
                  // onClick={onDelete}
                  // disabled={!order._id}
                  disabled
                  color='secondary'
                >
                  Удалить
                </Button>
              </Grid>
              <Grid item className={classes._MLAuto}>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label='Back'
                  variant='contained'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                  // disabled={!(order.image || croppedImg)}
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
      <AlertDialog
        title='Удалить?'
        text={order.title}
        isVisible={isVisibleDelete}
        onCancel={dialogCancel}
        onConfirm={dialogConfirm}
      />
    </MuiThemeProvider>
  )
}

OrderEdit.defaultProps = {
  order: {},
  categories: [],
  error: {},
}

export default OrderEdit
