import { withRouter } from 'react-router-dom'
import { compose, withProps, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import { getLandings } from '../../../../redux/modules/landing'
import {
  getCategories,
  setSelectedCategory,
} from '../../../../redux/modules/category'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// tools
import isEmpty from '../../../../helpers/is-empty'

import LandingList from './LandingList'

export default compose(
  connect(
    ({ landingStore }) => {
      const { landings } = landingStore

      return {
        landings,
      }
    },
    { getLandings }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
  }),
  lifecycle({
    componentDidMount() {
      this.props.getLandings()
    },
    componentWillUnmount() {},
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(LandingList)
