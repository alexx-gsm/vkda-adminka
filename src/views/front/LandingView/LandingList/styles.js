export default (theme) => ({
  Wrap: {
    width: '100%',
    maxWidth: '960px',
  },
  Select: {
    // margin: theme.spacing(1),
    minWidth: '200px',
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(1),
      boxSizing: 'border-box',
    },
    [theme.breakpoints.down('xs')]: {
      background: theme.palette.grey[600],
    },
  },
  xsUnderlineWhite: {
    [theme.breakpoints.down('xs')]: {
      '&:before': {
        borderBottomColor: theme.palette.common.white + '!important',
      },
      '&:after': {
        borderBottomColor: theme.palette.common.white,
      },
    },
  },
  SelectInput: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    '& $typoSelectInputTitle': {
      [theme.breakpoints.down('xs')]: {
        color: theme.palette.common.white,
      },
    },
  },
  PaperDishes: {
    // marginBottom: theme.spacing(2),
    padding: 0,
  },
  typoSelectInputTitle: {
    lineHeight: 1,
    color: theme.palette.grey[600],
    fontSize: '1rem',
    textTransform: 'uppercase',
  },
  xsColorWhite: {
    [theme.breakpoints.down('xs')]: {
      color: theme.palette.common.white,
    },
  },
  ListSubTitle: {
    fontSize: '12px',
    fontStyle: 'italic',
    color: theme.palette.grey[600],
  },
})
