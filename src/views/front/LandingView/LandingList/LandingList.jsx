import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

// @material-ui components
import { Grid, Typography, Fab, TablePagination } from '@material-ui/core'
import { List, ListItem, IconButton } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-icons
import IconNavBefore from '@material-ui/icons/NavigateBefore'
import IconNavNext from '@material-ui/icons/NavigateNext'
import AddIcon from '@material-ui/icons/Add'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
import moment from 'moment'
import 'moment/locale/ru'

const LandingList = ({
  landings,
  handleClick,
  loading,
  error,
  classes,
  basePath,
  theme,
}) => {
  const [rowsPerPage, handleChangeRowsPerPage] = useState(5)
  const [page, handleChangePage] = useState(0)

  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <List>
            {landings.map((landing) => {
              return (
                <ListItem
                  key={landing._id}
                  button
                  divider
                  onClick={handleClick(landing._id)}
                >
                  <Grid container spacing={1} alignItems='baseline'>
                    <Grid item>
                      <Typography variant='h6'>
                        {landing.week} неделя
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography variant='caption'>
                        ({landing.days})
                      </Typography>
                    </Grid>
                  </Grid>
                </ListItem>
              )
            })}
          </List>

          {/* PAGER */}
          <Grid className={classes._Row}>
            <Grid
              className={classes._GridActions}
              container
              spacing={2}
              justify='space-between'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item>
                <Fab
                  aria-label='Add'
                  color='secondary'
                  size='medium'
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              <Grid item>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={landings.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={(e, page) => handleChangePage(page)}
                  onChangeRowsPerPage={(e) =>
                    handleChangeRowsPerPage(e.target.value)
                  }
                  labelRowsPerPage='Кол-во на стр.'
                />
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <React.Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </React.Fragment>
      )}
    </MuiThemeProvider>
  )
}

LandingList.defaultProps = {
  landings: [],
}

LandingList.propTypes = {
  loading: PropTypes.bool,
  classes: PropTypes.object,
}

export default LandingList
