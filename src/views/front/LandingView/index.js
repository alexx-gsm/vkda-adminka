import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const LandingList = lazy(() => import('./LandingList'))
const LandingEdit = lazy(() => import('./LandingEdit'))

const basePath = '/front/landing'

const LandingView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <LandingList basePath={basePath} theme={theme} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <LandingEdit basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default LandingView
