export default (theme) => ({
  PaperTabs: {
    marginTop: theme.spacing(3),
  },
  leftIcon: {
    marginRight: theme.spacing(2),
  },
  BlockTitle: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(1),
    color: theme.palette.textColor[800],
  },
  TabWrapper: {
    flexDirection: 'row-reverse',
    '& svg': {
      marginLeft: theme.spacing(2),
      color: theme.palette.secondary.main,
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
})
