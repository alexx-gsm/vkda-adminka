import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import { getCategories } from '../../../../redux/modules/category'
import { getDishes } from '../../../../redux/modules/dish'
import {
  getOneLanding,
  saveLanding,
  setLanding,
} from '../../../../redux/modules/landing'
// Hocs
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'
import moment from 'moment'
import 'moment/locale/ru'

import LandingEdit from './LandingEdit'

export default compose(
  connect(
    ({ landingStore, categoryStore, dishStore }) => {
      const { landing } = landingStore
      const { categories } = categoryStore
      const { dishes } = dishStore

      const filteredCategories = !isEmpty(categories)
        ? ['Салаты', 'Супы', 'Горячее', 'Гарниры'].map((catTitle) =>
            categories.find((c) => c.title === catTitle)
          )
        : []

      const filteredDishes =
        !isEmpty(filteredCategories) && !isEmpty(dishes)
          ? filteredCategories.reduce((acc, fc) => {
              return {
                ...acc,
                [fc.title]: dishes.filter((dish) => dish.categoryId === fc._id),
              }
            }, {})
          : {}

      return {
        landing,
        filteredCategories,
        filteredDishes,
        dishes,
      }
    },
    {
      getCategories,
      getDishes,
      getOneLanding,
      saveLanding,
      setLanding,
    }
  ),
  withRouter,
  withState('isOpenSelectWeekDialog', 'setOpenSelectWeekDialog', false),
  withState('selectedDays', 'setSelectedDays', []),
  withState('tab', 'setTab', 0),
  withState('complexDinners', 'setComplexDinners', {}),
  withState('favoriteDishes', 'setFavoriteDishes', {}),
  withState('isDinnerCompleted', 'setDinnerCompleted', {}),
  withHandlers({
    handleSelectWeekDialog: ({ setOpenSelectWeekDialog }) => (status) => () =>
      setOpenSelectWeekDialog(status),
    handleTabs: ({ setTab }) => (e, tab) => setTab(tab),
    handleSelectDish: ({
      complexDinners,
      setComplexDinners,
      favoriteDishes,
      isDinnerCompleted,
      setDinnerCompleted,
    }) => (tab, catTitle) => (event) => {
      const newComplexDinners = {
        ...complexDinners,
        [tab]: {
          ...complexDinners[tab],
          [catTitle]: event.target.value,
        },
      }
      setComplexDinners(newComplexDinners)

      const isComplited =
        Object.keys(newComplexDinners[tab]).length === 4 &&
        !isEmpty(favoriteDishes[tab])

      setDinnerCompleted({
        ...isDinnerCompleted,
        [tab]: isComplited,
      })
    },
    handleSelectFavoriteDish: ({
      favoriteDishes,
      setFavoriteDishes,
      complexDinners,
      isDinnerCompleted,
      setDinnerCompleted,
    }) => (tab) => (event) => {
      const newFavoriteDishes = {
        ...favoriteDishes,
        [tab]: event.target.value,
      }

      setFavoriteDishes(newFavoriteDishes)

      const isComplited =
        !isEmpty(complexDinners[tab]) &&
        Object.keys(complexDinners[tab]).length === 4 &&
        !isEmpty(newFavoriteDishes[tab])

      setDinnerCompleted({
        ...isDinnerCompleted,
        [tab]: isComplited,
      })
    },
    handleClear: ({
      complexDinners,
      setComplexDinners,
      favoriteDishes,
      setFavoriteDishes,
      isDinnerCompleted,
      setDinnerCompleted,
    }) => (tab) => () => {
      if (!isEmpty(complexDinners[tab])) {
        const { [tab]: undefined, ...clearedComplexDinner } = complexDinners
        setComplexDinners(clearedComplexDinner)
      }

      if (!isEmpty(favoriteDishes[tab])) {
        const { [tab]: undefined, ...clearedFavoriteDishes } = favoriteDishes
        setFavoriteDishes(clearedFavoriteDishes)
      }

      setDinnerCompleted({
        ...isDinnerCompleted,
        [tab]: false,
      })
    },
    onSave: ({
      landing,
      selectedDays,
      complexDinners,
      favoriteDishes,
      saveLanding,
      basePath,
      history,
    }) => () => {
      const landingData = {
        _id: landing._id,
        week: moment(selectedDays[0]).format('w'),
        days: `${moment(selectedDays[0]).format('L')} - ${moment(
          selectedDays[6]
        ).format('L')}`,
        complexDinners,
        favoriteDishes,
      }

      saveLanding(landingData, () => history.push(basePath))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (id) {
        this.props.getOneLanding(id)
      }

      this.props.getCategories()
      this.props.getDishes()
    },
    componentDidUpdate(prevProps) {
      if (isEmpty(prevProps.landing) && !isEmpty(this.props.landing)) {
        const { week, complexDinners, favoriteDishes } = this.props.landing
        const selectedDays = [...Array(7)].map((day, i) =>
          moment(week, 'w')
            .startOf('week')
            .add(i, 'day')
            .toDate()
        )
        this.props.setSelectedDays(selectedDays)
        this.props.setComplexDinners(complexDinners)
        this.props.setFavoriteDishes(favoriteDishes)

        const isComplited = [...Array(5)].reduce((acc, day, tab) => {
          return {
            ...acc,
            [tab]:
              !isEmpty(complexDinners[tab]) &&
              Object.keys(complexDinners[tab]).length === 4 &&
              !isEmpty(favoriteDishes[tab]),
          }
        }, {})
        this.props.setDinnerCompleted(isComplited)
      }
    },
    componentWillUnmount() {
      this.props.setLanding({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(LandingEdit)
