import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import SelectWeekDialog from '../../../../components/SelectWeekDialog'
// Material UI
import { Paper, Typography, TextField, Button } from '@material-ui/core'
import { Tabs, Tab } from '@material-ui/core'
import { Grid, InputLabel, Select, MenuItem } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-UI icon
import IconDateRange from '@material-ui/icons/DateRange'
import IconDone from '@material-ui/icons/Done'

// useful tools
import moment from 'moment'
import 'moment/locale/ru'
import isEmpty from '../../../../helpers/is-empty'
import { HOST } from '../../../../config'

const LandingEdit = ({
  dishes,
  filteredCategories,
  isDinnerCompleted,
  filteredDishes,
  complexDinners,
  isOpenSelectWeekDialog,
  selectedDays,
  setSelectedDays,
  tab,
  handleTabs,
  handleSelectWeekDialog,
  handleSelectDish,
  favoriteDishes,
  handleSelectFavoriteDish,
  handleClear,
  landing,
  error,
  classes,
  onSave,
  onDelete,

  loading,
  basePath,
  theme,
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <SelectWeekDialog
            selectedDays={selectedDays}
            setSelectedDays={setSelectedDays}
            isOpen={isOpenSelectWeekDialog}
            handleClose={handleSelectWeekDialog(false)}
          />

          <Button
            variant='contained'
            color='primary'
            className={classes.button}
            onClick={handleSelectWeekDialog(true)}
            fullWidth
          >
            <IconDateRange className={classes.leftIcon} />
            {isEmpty(selectedDays)
              ? 'выбрать неделю...'
              : moment(selectedDays[0]).format('w') +
                ` неделя (${moment(selectedDays[0]).format('L')} - ${moment(
                  selectedDays[6]
                ).format('L')})`}
          </Button>

          {!isEmpty(selectedDays) &&
            !isEmpty(filteredCategories) &&
            !isEmpty(filteredDishes) && (
              <Fragment>
                <Paper square className={classes.PaperTabs}>
                  <Tabs value={tab} onChange={handleTabs} variant='fullWidth'>
                    {[...Array(5)].map((day, i) => (
                      <Tab
                        key={i}
                        icon={isDinnerCompleted[i] ? <IconDone /> : null}
                        label={moment()
                          .day(i + 1)
                          .format('dd')}
                        wrapped
                        classes={{
                          wrapper: classes.TabWrapper,
                        }}
                      />
                    ))}
                  </Tabs>
                </Paper>

                {[...Array(5)].map(
                  (day, i) =>
                    tab === i && (
                      <Fragment key={i}>
                        <Typography
                          variant='h4'
                          className={classes.BlockTitle}
                          align='center'
                        >
                          Комплексный обед
                        </Typography>

                        {filteredCategories.map((cat) => {
                          const dishId =
                            !isEmpty(complexDinners[i]) &&
                            !isEmpty(complexDinners[i][cat.title])
                              ? complexDinners[i][cat.title]._id
                              : ''
                          return (
                            <TextField
                              key={`${cat.title}-${tab}`}
                              id={`${cat.title}-${tab}`}
                              select
                              label={cat.title}
                              value={
                                !isEmpty(dishId)
                                  ? filteredDishes[cat.title].find(
                                      (fd) => fd._id === dishId
                                    )
                                  : ''
                              }
                              onChange={handleSelectDish(i, cat.title)}
                              SelectProps={{
                                MenuProps: {
                                  className: classes.menu,
                                },
                              }}
                              margin='dense'
                              fullWidth
                            >
                              {filteredDishes[cat.title].map((dish) => (
                                <MenuItem key={dish._id} value={dish}>
                                  {dish.title}
                                </MenuItem>
                              ))}
                            </TextField>
                          )
                        })}

                        <Typography
                          variant='h4'
                          className={classes.BlockTitle}
                          align='center'
                        >
                          Блюдо дня
                        </Typography>
                        <TextField
                          id={`${tab}`}
                          select
                          value={
                            !isEmpty(favoriteDishes[i])
                              ? dishes.find(
                                  (d) => d._id === favoriteDishes[i]._id
                                )
                              : ''
                          }
                          onChange={handleSelectFavoriteDish(i)}
                          SelectProps={{
                            MenuProps: {
                              className: classes.menu,
                            },
                          }}
                          margin='dense'
                          fullWidth
                        >
                          {dishes.map((dish) => (
                            <MenuItem key={dish._id} value={dish}>
                              {dish.title}
                            </MenuItem>
                          ))}
                        </TextField>

                        <Grid className={classes._Row}>
                          <Grid
                            container
                            className={classes._GridActions}
                            spacing={1}
                            justify='flex-end'
                          >
                            <Grid item xs={12}>
                              <Button
                                onClick={handleClear(tab)}
                                variant='text'
                                disabled={
                                  (isEmpty(complexDinners[tab]) ||
                                    Object.keys(complexDinners[tab]).length ===
                                      0) &&
                                  isEmpty(favoriteDishes[tab])
                                }
                                color='secondary'
                                fullWidth
                              >
                                {`Очистить "${moment()
                                  .day(i + 1)
                                  .format('dddd')}"`}
                              </Button>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Fragment>
                    )
                )}
              </Fragment>
            )}

          <Grid className={classes._Row}>
            <Grid
              container
              className={classes._GridActions}
              spacing={1}
              justify='space-between'
            >
              {/* <Grid item>
                <Button
                  variant='text'
                  // onClick={onDelete}
                  // disabled={!order._id}
                  disabled
                  color='secondary'
                >
                  Удалить
                </Button>
              </Grid> */}
              <Grid item className={classes._MLAuto}>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label='Back'
                  variant='contained'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                  disabled={
                    !Object.keys(isDinnerCompleted).some(
                      (tab) => isDinnerCompleted[tab]
                    )
                  }
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
      {/* <AlertDialog
        title='Удалить?'
        text={dish.title}
        isVisible={isVisibleDelete}
        onCancel={dialogCancel}
        onConfirm={dialogConfirm}
      /> */}
    </MuiThemeProvider>
  )
}

LandingEdit.defaultProps = {
  error: {},
}

export default LandingEdit
