import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withProps,
} from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getMenu,
  setMenu,
  saveMenu,
  deleteMenu,
} from '../../../../redux/modules/menu'
import { getCategories } from '../../../../redux/modules/category'
import { getDishes } from '../../../../redux/modules/dish'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'
import moment from 'moment'

import MenuEdit from './MenuEdit'

function readFile(file) {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => resolve(reader.result), false)
    reader.readAsDataURL(file)
  })
}

export default compose(
  connect(
    ({ menuStore, categoryStore, dishStore }) => {
      const { menu, error, loading } = menuStore
      const { categories } = categoryStore
      const { dishes } = dishStore
      const menuDishes =
        !isEmpty(menu) && menu.dishes && !isEmpty(dishes)
          ? menu.dishes.map((md) => dishes.find((d) => d._id === md))
          : []
      const dishCategory = !isEmpty(categories)
        ? categories.find((c) => c.title === 'Блюда')
        : null
      const dishCategories = dishCategory
        ? categories.filter((c) => c.rootId === dishCategory._id)
        : []
      return {
        menu,
        categories: dishCategories,
        dishes,
        menuDishes,
        error,
        loading,
      }
    },
    { getMenu, setMenu, saveMenu, deleteMenu, getCategories, getDishes }
  ),
  withRouter,
  withState('isVisibleAddForm', 'setVisibleAddForm', false),
  withState('isVisibleDelete', 'setIsVisibleDelete', false),
  withState('isVisibleDeleteMenu', 'setIsVisibleDeleteMenu', false),
  withState('dishToDelete', 'setDishToDelete', {}),
  withState('selectedCategory', 'setSelectedCategory', ''),
  withState('selectedDishes', 'setSelectedDishes', []),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withHandlers({
    handleDayChange: ({ menu, setMenu }) => (date) => {
      setMenu({
        ...menu,
        date: moment(date).format('L'),
      })
    },
    handleCopy: ({ menu, setMenu, basePath, history }) => () => {
      const { _id, date, ...newMenu } = menu
      setMenu(newMenu)
      history.push(`${basePath}/edit`)
    },
    handleAdd: ({
      menu,
      setMenu,
      selectedDishes,
      setVisibleAddForm,
      setSelectedCategory,
      setSelectedDishes,
    }) => () => {
      const dishes = menu.dishes ? menu.dishes : []
      setMenu({
        ...menu,
        dishes: [...dishes, ...selectedDishes],
      })
      setVisibleAddForm(false)
      setSelectedCategory('')
      setSelectedDishes('')
    },
    handleRemove: ({ dishes, setDishToDelete, setIsVisibleDelete }) => (
      _id
    ) => () => {
      setDishToDelete(dishes.find((d) => d._id === _id))
      setIsVisibleDelete(true)
    },
    handleCancel: ({
      setVisibleAddForm,
      setSelectedCategory,
      setSelectedDishes,
    }) => () => {
      setVisibleAddForm(false)
      setSelectedCategory('')
      setSelectedDishes('')
    },
    prepareMenu: ({ menuDishes, categories }) => () => {
      return categories.sort().reduce(
        (acc, cat) => {
          return {
            ...acc,
            [cat.title]: menuDishes.filter((md) => md.categoryId === cat._id),
          }
        },
        { count: menuDishes.length }
      )
    },
    onSave: ({ menu, saveMenu, basePath, history }) => () => {
      const date = menu.date ? menu.date : moment(menu.date).format('L')
      const week = moment(date, 'L').week()

      saveMenu(
        {
          ...menu,
          date,
          week,
        },
        () => history.push(basePath)
      )
    },
    onDelete: ({ setIsVisibleDelete }) => () => {
      setIsVisibleDelete(true)
    },
    dialogCancel: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(false),
    dialogConfirm: ({
      menu,
      setMenu,
      dishToDelete,
      setIsVisibleDelete,
    }) => () => {
      setMenu({
        ...menu,
        dishes: menu.dishes.filter((d_id) => d_id !== dishToDelete._id),
      })
      setIsVisibleDelete(false)
    },
    onDeleteMenu: ({ setIsVisibleDeleteMenu }) => () => {
      setIsVisibleDeleteMenu(true)
    },
    dialogCancelDeleteMenu: ({ setIsVisibleDeleteMenu }) => () =>
      setIsVisibleDeleteMenu(false),
    dialogConfirmDeleteMenu: ({ menu, deleteMenu, history, basePath }) => () =>
      deleteMenu(menu._id, () => history.push(basePath)),
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (id) {
        this.props.getMenu(id)
      }
      this.props.getCategories()
      this.props.getDishes()
    },
    componentWillUnmount() {
      this.props.setMenu('')
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(MenuEdit)
