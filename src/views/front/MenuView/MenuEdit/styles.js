export default (theme) => ({
  Wrap: {
    maxWidth: '960px',
  },
  Form: {
    marginBottom: theme.spacing(4),
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px',
    },
  },
  PaperAdd: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(3),
    paddingTop: theme.spacing(1),
    background: theme.palette.grey[100],
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
      paddingTop: 0,
    },
  },
  GridHeader: {
    padding: '10px 20px',
    marginTop: '10px',
    marginBottom: '20px',
    background: theme.palette.grey[600],
  },
  TypoHeader: {
    color: theme.palette.common.white,
    textTransform: 'uppercase',
  },
  IconButtonRemove: {
    marginRight: '15px',
    padding: 0,
  },
  ButtonPDF: {
    '& .link': {
      color: theme.palette.common.white,
      textDecoration: 'none',
    },
  },
  ListItem: {
    padding: 0,
    paddingTop: theme.spacing(1) / 2,
    paddingBottom: theme.spacing(1) / 2,
  },
  ButtonAdd: {
    [theme.breakpoints.down('xs')]: {
      alignSelf: 'center',
      marginTop: theme.spacing(3),
    },
  },
  GridActionsWrap: {
    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(3),
    },
  },
})
