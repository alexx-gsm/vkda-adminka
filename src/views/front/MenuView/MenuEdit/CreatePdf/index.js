import { compose, withProps } from 'recompose'

import CreatePdf from './CreatePdf'

export default compose(
  withProps(({ prepareMenu, date }) => {
    const { count, ...menu } = prepareMenu()
    return {
      menu,
      count,
      date,
      formatter: new Intl.NumberFormat('ru-ru', {
        style: 'currency',
        currency: 'RUB',
        minimumFractionDigits: 0,
      }),
    }
  })
)(CreatePdf)
