import React, { Fragment } from 'react'
import { Page, Text, View, Document, Image } from '@react-pdf/renderer'
import styles from './styles'
import { HOST } from '../../../../../config'
import isEmpty from '../../../../../helpers/is-empty'

const CreatePdf = ({ menu, count, date, type, formatter }) => {
  let i = 1

  const Section = ({ category }) => (
    <View style={styles.section}>
      <View style={styles.ViewCategory}>
        <Text style={styles.TextCategory}>{category}</Text>
      </View>
      {menu[category].map((dish) => (
        <View style={styles.row} key={dish._id}>
          {type === 'DELIVERY_VERSION' && (
            <Text style={styles.TextDishIndex}>{i++}.</Text>
          )}
          <View style={styles.ViewDishTitle}>
            <Text style={styles.TextDishTitle}>{dish.title}</Text>
            <Text style={styles.TextDishRecipe}>{dish.recipe}</Text>
          </View>
          {!isEmpty(dish.weight) ? (
            <Fragment>
              {type === 'DELIVERY_VERSION' ? (
                <Text style={styles.TextWeight}>{dish.weight}</Text>
              ) : (
                <Text style={styles.TextWeight}>
                  {dish.extraPrices
                    ? dish.extraPrices.map((ep) =>
                        ep.weight ? `${ep.weight} / ` : ''
                      )
                    : ''}
                  {dish.weight2}
                </Text>
              )}

              <Text style={styles.TextWeight}>/</Text>
            </Fragment>
          ) : (
            <Text style={styles.TextWeight}>/</Text>
          )}
          <Text style={styles.text}>
            {type === 'DELIVERY_VERSION' ? (
              formatter.format(dish.price)
            ) : (
              <Fragment>
                {dish.extraPrices
                  ? dish.extraPrices.map((ep) =>
                      ep.price ? `${formatter.format(ep.price)} / ` : ''
                    )
                  : ''}
                {formatter.format(dish.price2)}
              </Fragment>
            )}
          </Text>
        </View>
      ))}
    </View>
  )

  return (
    <Document>
      <Page size='A4' style={styles.page}>
        {type === 'DELIVERY_VERSION' && (
          <View style={styles.ViewHeader}>
            <View>
              <Image
                src={`${HOST}/images/menu/logo-bw.jpg`}
                style={styles.ImageLogo}
              />
            </View>
            <View style={styles.ViewMenu}>
              <Text style={styles.TextMenu}>МЕНЮ</Text>
            </View>
            <View style={styles.ViewHeaderInfo}>
              <Text style={styles.TextData}>{date}</Text>
              <View style={styles.ViewTime}>
                <Text style={styles.TextTimeLabel}>Прием заказов до</Text>
                <Text style={styles.TextTime}>10:30</Text>
              </View>
            </View>
          </View>
        )}

        {type === 'CANTEEN_VERSION' && (
          <View style={styles.ViewHeader}>
            <View style={{ width: 200 }}>
              <Text style={styles.TextPhone}>99-88-80</Text>
              <Text style={styles.TextAppeal}>Заказ обедов</Text>
            </View>
            <View>
              <Text style={styles.TextMenu}>МЕНЮ</Text>
            </View>
            <View style={styles.ViewHeaderInfo}>
              <Text style={styles.TextData}>{date}</Text>
            </View>
          </View>
        )}

        {!isEmpty(menu['Салаты']) && <Section category='Салаты' />}

        {!isEmpty(menu['Супы']) && <Section category='Супы' />}

        {!isEmpty(menu['Горячее']) && <Section category='Горячее' />}

        {count > 18 ? (
          <View style={styles.ViewRow}>
            <View style={styles.ViewRowItem}>
              {!isEmpty(menu['Гарниры']) && <Section category='Гарниры' />}
              {!isEmpty(menu['Выпечка']) && <Section category='Выпечка' />}
            </View>
            <View style={styles.ViewRowItem}>
              {!isEmpty(menu['Соусы']) && <Section category='Соусы' />}
              {!isEmpty(menu['Напитки']) && <Section category='Напитки' />}
            </View>
          </View>
        ) : (
          <View>
            {!isEmpty(menu['Гарниры']) && <Section category='Гарниры' />}

            {!isEmpty(menu['Выпечка']) && <Section category='Выпечка' />}

            {!isEmpty(menu['Напитки']) && <Section category='Напитки' />}

            {!isEmpty(menu['Соусы']) && <Section category='Соусы' />}
          </View>
        )}

        {/* INFO */}
        <View style={styles.ViewInfo}>
          <View style={styles.ViewRowItem}>
            <View style={styles.ViewInfoRow}>
              <Text style={styles.TextInfoTitle}>Инстаграм:</Text>
              <Text style={styles.TextInfo}>@vkusnoda22</Text>
            </View>
          </View>
          <View style={styles.ViewRowItem}>
            <View style={styles.ViewInfoRow}>
              <Text style={styles.TextInfoTitle}>Телефон:</Text>
              <Text style={styles.TextInfo}>99-88-80</Text>
            </View>
            <View style={styles.ViewInfoRow}>
              <Text style={styles.TextInfoTitle}>WhatsApp:</Text>
              <Text style={styles.TextInfo}>8-964-086-8880</Text>
            </View>
          </View>
          <View style={styles.ViewRowItem}>
            <View style={styles.ViewInfoRow}>
              <Text style={styles.TextInfoTitle}>Сайт:</Text>
              <Text style={styles.TextInfo}>www.vk-da.ru</Text>
            </View>
            <View style={styles.ViewInfoRow}>
              <Text style={styles.TextInfoTitle}>Почта:</Text>
              <Text style={styles.TextInfo}>info@vk-da.ru</Text>
            </View>
          </View>
        </View>
      </Page>
    </Document>
  )
}

export default CreatePdf
