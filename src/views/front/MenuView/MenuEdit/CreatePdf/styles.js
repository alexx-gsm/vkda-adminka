import { StyleSheet, Font } from '@react-pdf/renderer'
import { HOST } from '../../../../../config'

// Register font
Font.register(`${HOST}/fonts/Roboto-Regular.ttf`, { family: 'Roboto' })
Font.register(`${HOST}/fonts/Segoe.ttf`, { family: 'Segoe' })

export default StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: '#FFF',
    fontFamily: 'Roboto',
  },
  // HEADER
  ImageLogo: {
    width: 200,
  },
  ViewHeader: {
    marginBottom: 20,
    padding: '10 30',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  TextPhone: {
    fontSize: 28,
  },
  TextAppeal: {
    fontSize: 14,
  },
  ViewMenu: {},
  TextMenu: {
    fontSize: 40,
  },
  ViewHeaderInfo: {
    width: 200,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  TextData: { fontSize: 12, marginBottom: 5 },
  ViewTime: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  TextTimeLabel: { fontSize: 12, paddingRight: 5 },
  TextTime: { fontSize: 18, borderBottom: 1, borderColor: '#f23' },
  section: {
    padding: '5 30',
  },
  ViewCategory: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // padding: 10
  },
  rowTitle: {
    // displey: 'flex',
    // justifyContent: 'center'
  },
  TextCategory: {
    fontFamily: 'Segoe',
    fontSize: 12,
    textTransform: 'uppercase',
    marginLeft: 5,
  },
  ImageIcon: {
    width: 30,
  },
  row: {
    displey: 'flex',
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    borderBottom: 1,
    borderColor: '#b2b2b2',
    padding: '2 0',
  },
  TextDishIndex: {
    fontSize: 12,
    width: 20,
    // paddingRight: 5
  },
  ViewDishTitle: {
    marginRight: 'auto',
  },
  TextDishTitle: {
    fontSize: 14,
  },
  TextDishRecipe: {
    fontSize: 10,
    color: '#999',
  },
  TextWeight: {
    fontSize: 10,
    color: '#999',
    paddingRight: 5,
  },
  text: {
    fontSize: 12,
  },
  ViewRow: {
    flexDirection: 'row',
    paddingBottom: 5,
  },
  ViewInfo: {
    flexDirection: 'row',
    marginTop: 'auto',
    padding: 10,
    borderTop: '1 solid #b2b2b2',
  },
  ViewRowItem: {
    flex: 1,
  },
  ImageInfoIcon: { width: 20, maxWidth: '100%' },
  TextInfoTitle: {
    width: 70,
    textAlign: 'right',
    fontSize: 12,
    color: '#999',
  },
  TextInfo: {
    marginLeft: 5,
    fontSize: 14,
  },
  ViewInfoRow: {
    flexDirection: 'row',
    paddingBottom: 5,
    alignItems: 'center',
  },
  ViewRecipe: {
    marginRight: 'auto',
  },
})
