import React, { Fragment, useState } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { PDFDownloadLink } from '@react-pdf/renderer'
// Components
import DayPicker from './DayPicker'
import AlertDialog from '../../../../components/AlertDialog'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import { Paper, Grid, Typography, TextField } from '@material-ui/core'
import { FormControl, InputLabel, Select, Input } from '@material-ui/core'
import { Checkbox } from '@material-ui/core'
import { List, ListItem, MenuItem, ListItemText } from '@material-ui/core'
import { Button, Fab, IconButton } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
import CreatePdf from './CreatePdf'
import moment from 'moment'
// icons
import RemoveIcon from '@material-ui/icons/RemoveCircleOutline'
import AddIcon from '@material-ui/icons/Add'
import CopyIcon from '@material-ui/icons/FileCopy'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
}

const MenuEdit = ({
  menu,
  formatter,
  handleDayChange,
  categories,
  selectedCategory,
  setSelectedCategory,
  dishes,
  menuDishes,
  selectedDishes,
  setSelectedDishes,
  isVisibleAddForm,
  setVisibleAddForm,
  handleCopy,
  handleAdd,
  handleCancel,
  handleRemove,
  prepareMenu,
  dishToDelete,
  isVisibleDelete,
  dialogCancel,
  dialogConfirm,
  isVisibleDeleteMenu,
  onDeleteMenu,
  dialogCancelDeleteMenu,
  dialogConfirmDeleteMenu,
  error,
  classes,
  onSave,
  onDelete,
  loading,
  basePath,
  theme,
}) => {
  const [showPdf, setShowPdf] = useState({ delivery: false, canteen: false })

  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <Fragment>
            <Grid
              container
              spacing={1}
              alignItems='flex-end'
              justify='space-between'
            >
              <Grid item>
                <DayPicker
                  format='D MMMM YYYY'
                  value={menu.date}
                  onDayClick={handleDayChange}
                  margin='normal'
                  label='Дата меню'
                />
              </Grid>
              <Grid item>
                <Button
                  variant='text'
                  onClick={onDeleteMenu}
                  disabled={!menu._id}
                  color='secondary'
                >
                  Удалить
                </Button>
              </Grid>
            </Grid>

            <Grid
              className={classes.GridHeader}
              container
              justify='space-between'
              alignItems='center'
            >
              <Typography className={classes.TypoHeader} variant='h5'>
                Блюда
              </Typography>
              <Fab
                color='secondary'
                size='small'
                disabled={!menu._id}
                onClick={handleCopy}
              >
                <CopyIcon />
              </Fab>
            </Grid>

            <Fragment>
              {categories &&
                !isEmpty(menuDishes) &&
                categories.map((c) => {
                  const filteredDishes = menuDishes.filter((d) =>
                    d ? d.categoryId === c._id : false
                  )
                  if (!isEmpty(filteredDishes)) {
                    return (
                      <Fragment key={c._id}>
                        <Typography
                          variant='h6'
                          align='center'
                          className={classes._ListSubTitle}
                        >
                          {c.title}
                        </Typography>
                        <List className={classes.PaperDishes}>
                          {filteredDishes.map((cd) => (
                            <ListItem
                              key={cd._id}
                              divider
                              classes={{
                                root: classes.ListItem,
                              }}
                            >
                              <IconButton
                                aria-label='Delete'
                                color='default'
                                onClick={handleRemove(cd._id)}
                                classes={{
                                  root: classes.IconButtonRemove,
                                }}
                              >
                                <RemoveIcon fontSize='large' />
                              </IconButton>
                              <Grid
                                container
                                spacing={2}
                                justify='space-between'
                                alignItems='center'
                                wrap='nowrap'
                              >
                                <Grid item>
                                  <Typography
                                    variant='h6'
                                    align='left'
                                    className={classes._ListTitle}
                                  >
                                    {cd.title}
                                  </Typography>
                                </Grid>
                                <Grid item>
                                  <Typography
                                    variant='h6'
                                    align='right'
                                    className={classes._ListPrice}
                                  >
                                    {cd.price ? formatter.format(cd.price) : ''}
                                  </Typography>
                                </Grid>
                              </Grid>
                            </ListItem>
                          ))}
                        </List>
                      </Fragment>
                    )
                  }
                })}

              {isVisibleAddForm ? (
                <Paper square className={classes.PaperAdd}>
                  {categories && (
                    <TextField
                      select
                      margin='normal'
                      id='category'
                      label='Категория'
                      value={isEmpty(selectedCategory) ? '' : selectedCategory}
                      onChange={(e) => setSelectedCategory(e.target.value)}
                      className={classes.Select}
                      InputProps={{
                        classes: {
                          input: classes.SelectInput,
                          underline: classes.xsUnderlineWhite,
                        },
                      }}
                      SelectProps={{
                        classes: {
                          icon: classes.xsColorWhite,
                        },
                      }}
                      error={Boolean(error.category)}
                      helperText={error.category ? error.category : null}
                      fullWidth
                    >
                      {categories.map((category) => {
                        return (
                          <MenuItem key={category._id} value={category._id}>
                            {category.title}
                          </MenuItem>
                        )
                      })}
                    </TextField>
                  )}

                  {selectedCategory !== '' && (
                    <FormControl className={classes.formControl} fullWidth>
                      <InputLabel htmlFor='select-multiple-checkbox'>
                        Блюда
                      </InputLabel>
                      <Select
                        multiple
                        value={!isEmpty(selectedDishes) ? selectedDishes : []}
                        onChange={(e) => setSelectedDishes(e.target.value)}
                        input={<Input id='select-multiple-checkbox' />}
                        renderValue={(selected) => {
                          const selectedTitleArray = selected.map(
                            (s) => dishes.find((d) => d._id === s)['title']
                          )

                          return selectedTitleArray.join(', ')
                        }}
                        MenuProps={MenuProps}
                      >
                        {dishes
                          .filter((d) => d.categoryId === selectedCategory)
                          .map((dish) => (
                            <MenuItem key={dish._id} value={dish._id}>
                              <Checkbox
                                checked={selectedDishes.indexOf(dish._id) > -1}
                              />
                              <ListItemText
                                primary={
                                  dishes.find((d) => d._id === dish._id).title
                                }
                              />
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  )}

                  <Grid item container spacing={2} className={classes._PT3}>
                    <Grid item>
                      <Button
                        variant='contained'
                        color='secondary'
                        disabled={isEmpty(selectedDishes)}
                        onClick={handleAdd}
                      >
                        Добавить
                      </Button>
                    </Grid>
                    <Grid item>
                      <Button onClick={handleCancel}>Отмена</Button>
                    </Grid>
                  </Grid>
                </Paper>
              ) : (
                <Grid className={classes._Row}>
                  <Grid
                    container
                    spacing={1}
                    justify='space-between'
                    className={classes._GridActions}
                  >
                    {!isEmpty(menuDishes) && !isEmpty(categories) && (
                      <Fragment>
                        <Grid container spacing={1} className={classes._MT2}>
                          <Grid item>
                            <Button
                              variant={
                                showPdf.delivery ? 'contained' : 'outlined'
                              }
                              color='secondary'
                              className={classes.ButtonPDF}
                              fullWidth
                              onClick={() =>
                                setShowPdf({ delivery: true, canteen: false })
                              }
                            >
                              Доставка
                            </Button>
                          </Grid>
                          <Grid item>
                            <Button
                              variant={
                                showPdf.canteen ? 'contained' : 'outlined'
                              }
                              color='secondary'
                              className={classes.ButtonPDF}
                              fullWidth
                              onClick={() =>
                                setShowPdf({ delivery: false, canteen: true })
                              }
                            >
                              Столовая
                            </Button>
                          </Grid>
                        </Grid>

                        {(showPdf.delivery || showPdf.canteen) && (
                          <Grid className={classes._MT2}>
                            <PDFDownloadLink
                              document={
                                <CreatePdf
                                  prepareMenu={prepareMenu}
                                  date={moment(menu.date, 'L').format(
                                    'D MMMM YYYY'
                                  )}
                                  type={
                                    showPdf.delivery
                                      ? 'DELIVERY_VERSION'
                                      : 'CANTEEN_VERSION'
                                  }
                                />
                              }
                              className='link'
                              fileName={
                                showPdf.delivery
                                  ? `${moment(menu.date, 'L').format(
                                      'D_MMMM_YYYY'
                                    )}.pdf`
                                  : `${moment(menu.date, 'L').format(
                                      'D_MMMM_YYYY_столовая'
                                    )}.pdf`
                              }
                            >
                              {({ loading }) => {
                                return loading ? (
                                  'Loading ...'
                                ) : (
                                  <Button variant='text' fullWidth>
                                    скачать
                                  </Button>
                                )
                              }}
                            </PDFDownloadLink>
                          </Grid>
                        )}
                      </Fragment>
                    )}
                  </Grid>
                </Grid>
              )}
            </Fragment>

            {!isVisibleAddForm && (
              <Grid className={classes.GridActionsWrap}>
                <Grid
                  container
                  className={classes._GridActions}
                  spacing={1}
                  justify='space-between'
                >
                  <Grid item className={classes.ButtonAdd}>
                    <Fab
                      aria-label='Add'
                      color='secondary'
                      size='medium'
                      onClick={() => setVisibleAddForm(true)}
                    >
                      <AddIcon />
                    </Fab>
                  </Grid>
                  <Grid item className={classes._MLAuto}>
                    <Button
                      to={basePath}
                      component={Link}
                      aria-label='Back'
                      variant='contained'
                      fullWidth
                    >
                      Отмена
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      onClick={onSave}
                      aria-label='Save'
                      variant='contained'
                      color='primary'
                      fullWidth
                    >
                      Сохранить
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            )}

            <AlertDialog
              title='Удалить?'
              text={dishToDelete.title}
              isVisible={isVisibleDelete}
              onCancel={dialogCancel}
              onConfirm={dialogConfirm}
            />
            <AlertDialog
              title='Удалить?'
              text='Меню'
              isVisible={isVisibleDeleteMenu}
              onCancel={dialogCancelDeleteMenu}
              onConfirm={dialogConfirmDeleteMenu}
            />
          </Fragment>
        ) : (
          <Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

MenuEdit.defaultProps = {
  dish: {},
  categories: [],
  error: {},
}

MenuEdit.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
}

export default MenuEdit
