import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const MenuList = lazy(() => import('./MenuList'))
const MenuEdit = lazy(() => import('./MenuEdit'))

const basePath = '/front/menus'

const MenuView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <MenuList basePath={basePath} theme={theme} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <MenuEdit basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default MenuView
