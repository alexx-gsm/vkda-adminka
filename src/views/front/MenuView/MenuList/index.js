import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withHandlers,
  lifecycle,
  withState
} from 'recompose'
import { connect } from 'react-redux'
// AC
import { getMenus } from '../../../../redux/modules/menu'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import CommonStyles from '../../../../styles/CommonStyles'
import styles from './styles'

import MenuList from './MenuList'

export default compose(
  connect(
    ({ menuStore }) => {
      const { menus, loading, error } = menuStore

      return {
        menus,
        loading,
        error
      }
    },
    { getMenus }
  ),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => id => () =>
      history.push(`${basePath}/edit/${id}`),

    onReload: ({}) => () => {}
  }),
  lifecycle({
    componentDidMount() {
      this.props.getMenus()
    }
  }),
  withStyles(
    theme => ({
      ...CommonStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(MenuList)
