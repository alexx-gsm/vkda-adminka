import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
// @material-ui components
import { Grid, Typography, Fab, TablePagination } from '@material-ui/core'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
import AddIcon from '@material-ui/icons/Add'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
import moment from 'moment'
import 'moment/locale/ru'

const MenuList = ({
  menus,
  categories,
  selectedCategory,
  handleFilter,
  basePath,
  formatter,
  loading,
  handleClick,
  error,
  classes,
  theme,
}) => {
  const [rowsPerPage, handleChangeRowsPerPage] = useState(5)
  const [page, handleChangePage] = useState(0)

  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <List>
            {menus.map((menu) => {
              return (
                <ListItem
                  key={menu._id}
                  button
                  divider
                  onClick={handleClick(menu._id)}
                >
                  <Grid container spacing={1}>
                    <Grid item>
                      <Typography variant='h6'>
                        {moment(menu.date, 'L').format('LL')}
                      </Typography>
                    </Grid>
                  </Grid>
                </ListItem>
              )
            })}
          </List>

          {/* PAGER */}
          <Grid className={classes._Row}>
            <Grid
              className={classes._GridActions}
              container
              spacing={2}
              justify='space-between'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item>
                <Fab
                  aria-label='Add'
                  color='secondary'
                  size='medium'
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              <Grid item>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={menus.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={(e, page) => handleChangePage(page)}
                  onChangeRowsPerPage={(e) =>
                    handleChangeRowsPerPage(e.target.value)
                  }
                  labelRowsPerPage='Кол-во на стр.'
                />
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
    </MuiThemeProvider>
  )
}

MenuList.defaultProps = {
  dishes: [],
}

MenuList.propTypes = {
  dishes: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  classes: PropTypes.object,
}

export default MenuList
