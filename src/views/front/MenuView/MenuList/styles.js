export default (theme) => ({
  Wrap: {
    width: '100%',
    maxWidth: '960px',
  },
  Select: {
    // margin: theme.spacing(1),
    minWidth: '200px',
  },
  xsUnderlineWhite: {
    [theme.breakpoints.down('xs')]: {
      '&:before': {
        borderBottomColor: theme.palette.common.white + '!important',
      },
      '&:after': {
        borderBottomColor: theme.palette.common.white,
      },
    },
  },
  SelectInput: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    '& $typoSelectInputTitle': {
      [theme.breakpoints.down('xs')]: {
        color: theme.palette.common.white,
      },
    },
  },
  PaperDishes: {
    marginBottom: theme.spacing(2),
    background: theme.palette.grey[50],
  },
  typoSelectInputTitle: {
    lineHeight: 1,
    color: theme.palette.grey[600],
    fontSize: '1rem',
    textTransform: 'uppercase',
  },
  xsColorWhite: {
    [theme.breakpoints.down('xs')]: {
      color: theme.palette.common.white,
    },
  },
})
