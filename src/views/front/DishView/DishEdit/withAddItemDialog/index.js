import React from 'react'
import AddItemDialog from './AddItemDialog'

const withAddItemDialog = Component => ({
  modalTitle,
  modalText,
  isVisible,
  setVisible,
  item,
  setItem,
  cancelDialog,
  confirmDialog,
  ...props
}) => (
  <React.Fragment>
    <Component {...props} />
    <AddItemDialog
      title={modalTitle}
      text={modalText}
      isVisible={isVisible}
      item={item}
      handleChange={setItem}
      onCancel={cancelDialog}
      onConfirm={confirmDialog}
    />
  </React.Fragment>
)

export default withAddItemDialog
