import React from 'react'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from '@material-ui/core'
import { TextField, Button } from '@material-ui/core'

const AddItemDialog = ({
  isVisible,
  title,
  text,
  item,
  handleChange,
  onCancel,
  onConfirm
}) => {
  return (
    <Dialog
      open={isVisible}
      onClose={onCancel}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{text}</DialogContentText>
        <TextField
          autoFocus
          margin='dense'
          id='item'
          fullWidth
          value={item}
          onChange={e => handleChange(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color='primary'>
          Отмена
        </Button>
        <Button onClick={onConfirm} color='primary' variant='contained'>
          Добавить
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default AddItemDialog
