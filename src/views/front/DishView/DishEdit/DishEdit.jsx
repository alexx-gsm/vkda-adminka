import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Cropper from 'react-easy-crop'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import AlertDialog from '../../../../components/AlertDialog'
// Material UI
import { Grid, Typography, TextField } from '@material-ui/core'
import { Fab, Button, IconButton } from '@material-ui/core'
import { Paper, InputAdornment, MenuItem } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-UI/lab
import Slider from '@material-ui/lab/Slider'
// @material-icons
import AddIcon from '@material-ui/icons/Add'
import DeleteIcon from '@material-ui/icons/Delete'
// React icons kit
import { Icon } from 'react-icons-kit'
import { rub } from 'react-icons-kit/fa/rub'
import { balanceScale } from 'react-icons-kit/fa/balanceScale'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
import { HOST } from '../../../../config'

const DishEdit = ({
  dish,
  noImageSrc,
  categories,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  handleDialog,
  imgSrc,
  croppedImg,
  crop,
  zoom,
  imgName,
  handleFileChange,
  setCrop,
  onCropComplete,
  setZoom,
  handleCropImage,
  isVisibleDelete,
  dialogCancel,
  dialogConfirm,
  extraPrices,
  handleExtraPrices,
  handleDeleteExtraPrices,
  onChangeExtraPrices,
  formatter,
  loading,
  basePath,
  theme,
}) => {
  console.log('extraPrices', extraPrices)
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          {/* <TopPanel title='Блюдо' /> */}

          <form
            noValidate
            className={classNames(classes._Form, classes.Form)}
            autoComplete='off'
          >
            {!imgSrc && !croppedImg && (
              <Fragment>
                <div className={classes.Image}>
                  <img
                    src={dish.image ? HOST + dish.image : HOST + noImageSrc}
                    alt={dish.title}
                  />
                </div>

                <Grid
                  container
                  alignItems='center'
                  spacing={1}
                  className={classes.ButtonUpload}
                >
                  <Grid item>
                    <input
                      accept='file/*'
                      name='upload'
                      className={classes.Upload}
                      id='upload'
                      type='file'
                      onChange={handleFileChange}
                    />
                    <label htmlFor='upload'>
                      <Button
                        variant='contained'
                        color='secondary'
                        component='span'
                        className={classes.UploadButton}
                      >
                        Загрузить
                      </Button>
                    </label>
                  </Grid>
                  <Grid item>
                    <Typography variant='caption'>{imgName}</Typography>
                  </Grid>
                </Grid>
              </Fragment>
            )}
            {imgSrc && (
              <Fragment>
                <Button
                  onClick={handleCropImage}
                  variant='contained'
                  color='primary'
                >
                  Обрезать
                </Button>
                <div className={classes.CropperWrap}>
                  <Cropper
                    image={imgSrc}
                    crop={crop}
                    zoom={zoom}
                    aspect={16 / 9}
                    onCropChange={setCrop}
                    onCropComplete={onCropComplete}
                    onZoomChange={setZoom}
                  />
                </div>
                <div className={classes.CropperControls}>
                  <Slider
                    value={zoom}
                    min={1}
                    max={3}
                    step={0.1}
                    aria-labelledby='Zoom'
                    onChange={(e, zoom) => setZoom(zoom)}
                    classes={{ container: classes.CropperSlider }}
                  />
                </div>
              </Fragment>
            )}
            {croppedImg && !imgSrc && (
              <div className={classes.Image}>
                <img src={croppedImg} alt='Cropped' className={classes.img} />
              </div>
            )}
            <TextField
              fullWidth
              id='title'
              label='Название'
              value={isEmpty(dish.title) ? '' : dish.title}
              onChange={onChange('title')}
              margin='normal'
              className={classes.textName}
              error={Boolean(error.title)}
              helperText={error.title ? error.title : null}
            />
            <Grid container spacing={2} alignItems='flex-end'>
              <Grid item xs>
                <TextField
                  select
                  fullWidth
                  id='category'
                  label='Категория'
                  value={isEmpty(dish.categoryId) ? '' : dish.categoryId}
                  onChange={onChange('categoryId')}
                  margin='none'
                  className={classes.wrapTitle}
                  InputProps={{
                    className: classes.Title,
                  }}
                  error={Boolean(error.category)}
                  helperText={error.category ? error.category : null}
                >
                  {categories.map((category) => {
                    return (
                      <MenuItem key={category._id} value={category._id}>
                        <Grid
                          container
                          alignItems='baseline'
                          className={classes.gridType}
                          direction='row'
                          wrap='wrap'
                        >
                          <Typography
                            className={classes.typoSelectInputTitle}
                            variant='h6'
                          >
                            {category.title}
                          </Typography>
                        </Grid>
                      </MenuItem>
                    )
                  })}
                </TextField>
              </Grid>
              <Grid item>
                <Button
                  onClick={handleDialog}
                  variant='contained'
                  color='secondary'
                  disabled
                >
                  Добавить
                </Button>
              </Grid>
            </Grid>

            <Paper className={classes.PaperPrices}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id='ptice'
                    label='Цена сайт/доставка'
                    value={isEmpty(dish.price) ? '' : dish.price}
                    onChange={onChange('price')}
                    type='number'
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon icon={rub} />
                        </InputAdornment>
                      ),
                    }}
                    error={error.value}
                    helperText={error.value ? error.value : null}
                    margin='normal'
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id='weight'
                    label='Размерность'
                    value={isEmpty(dish.weight) ? '' : dish.weight}
                    onChange={onChange('weight')}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon size={18} icon={balanceScale} />
                        </InputAdornment>
                      ),
                    }}
                    margin='normal'
                  />
                </Grid>
              </Grid>

              <Grid container spacing={2} justify='center'>
                <Grid item>
                  <Fab
                    aria-label='Add'
                    size='small'
                    color='primary'
                    disabled
                    onClick={handleExtraPrices}
                  >
                    <AddIcon />
                  </Fab>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id='price2'
                    label='Цена, столовая'
                    value={isEmpty(dish.price2) ? '' : dish.price2}
                    onChange={onChange('price2')}
                    type='number'
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon icon={rub} />
                        </InputAdornment>
                      ),
                    }}
                    margin='normal'
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id='weight2'
                    label='Размерность, столовая'
                    value={isEmpty(dish.weight2) ? '' : dish.weight2}
                    onChange={onChange('weight2')}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon size={18} icon={balanceScale} />
                        </InputAdornment>
                      ),
                    }}
                    margin='normal'
                  />
                </Grid>
              </Grid>
              {!isEmpty(extraPrices) &&
                extraPrices.map((extra, i) => {
                  return (
                    <Grid key={i} container spacing={2} alignItems='flex-end'>
                      <Grid item xs={12} sm={6}>
                        <TextField
                          fullWidth
                          id='price2'
                          label={`Цена-${i + 2}, столовая`}
                          value={
                            !isEmpty(extraPrices[i].price)
                              ? extraPrices[i].price
                              : ''
                          }
                          onChange={onChangeExtraPrices(i)}
                          name='price'
                          type='number'
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position='end'>
                                <Icon icon={rub} />
                              </InputAdornment>
                            ),
                          }}
                          margin='dense'
                        />
                      </Grid>
                      <Grid item xs={12} sm={5}>
                        <TextField
                          fullWidth
                          id='weight2'
                          label={`Размерность-${i + 2}, столовая`}
                          value={
                            !isEmpty(extraPrices[i].weight)
                              ? extraPrices[i].weight
                              : ''
                          }
                          onChange={onChangeExtraPrices(i)}
                          name='weight'
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position='end'>
                                <Icon size={18} icon={balanceScale} />
                              </InputAdornment>
                            ),
                          }}
                          margin='dense'
                        />
                      </Grid>
                      <Grid item xs={12} sm={1}>
                        <IconButton
                          aria-label='Delete'
                          className={classes.margin}
                          color='secondary'
                          onClick={handleDeleteExtraPrices(i)}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </Grid>
                    </Grid>
                  )
                })}

              <Grid container spacing={2} justify='center'>
                <Grid item>
                  <Fab
                    aria-label='Add'
                    size='small'
                    color='primary'
                    onClick={handleExtraPrices}
                  >
                    <AddIcon />
                  </Fab>
                </Grid>
              </Grid>

              {!isEmpty(extraPrices) && (
                <Grid
                  className={classes._MT3}
                  container
                  spacing={1}
                  alignItems='center'
                  justify='space-around'
                >
                  <Grid item>
                    <Typography variant='h5'>
                      {extraPrices.map((ep) =>
                        ep.price ? `${formatter.format(ep.price)} / ` : ''
                      )}
                      {formatter.format(dish.price2)}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant='h6'>
                      {extraPrices.map((ep) =>
                        ep.weight ? `${ep.weight} / ` : ''
                      )}
                      {dish.weight2}
                    </Typography>
                  </Grid>
                </Grid>
              )}
            </Paper>
            <TextField
              fullWidth
              id='recipe'
              label='Состав'
              value={isEmpty(dish.recipe) ? '' : dish.recipe}
              onChange={onChange('recipe')}
              margin='normal'
            />
            <TextField
              id='comment'
              label='Комментарий'
              multiline
              rows='2'
              className={classes.textField}
              value={isEmpty(dish.comment) ? '' : dish.comment}
              onChange={onChange('comment')}
              margin='normal'
              fullWidth
            />
            <TextField
              id='link'
              label='Ссылка'
              className={classes.textField}
              value={isEmpty(dish.link) ? '' : dish.link}
              onChange={onChange('link')}
              margin='normal'
              fullWidth
            />
          </form>

          <Grid className={classNames(classes._Row, classes._PB)}>
            <Grid
              container
              className={classes._GridActions}
              spacing={1}
              justify='space-between'
            >
              <Grid item>
                <Button
                  variant='text'
                  onClick={onDelete}
                  disabled={!dish._id}
                  color='secondary'
                  fullWidth
                >
                  Удалить
                </Button>
              </Grid>
              <Grid item className={classes._MLAuto}>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label='Back'
                  variant='contained'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                  // disabled={!(dish.image || croppedImg)}
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
      <AlertDialog
        title='Удалить?'
        text={dish.title}
        isVisible={isVisibleDelete}
        onCancel={dialogCancel}
        onConfirm={dialogConfirm}
      />
    </MuiThemeProvider>
  )
}

DishEdit.defaultProps = {
  dish: {},
  categories: [],
  error: {},
}

export default DishEdit
