import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withProps,
} from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getOneDish,
  setDish,
  saveDish,
  deleteDish,
} from '../../../../redux/modules/dish'
import { getCategories, addCategory } from '../../../../redux/modules/category'
// Hocs
import withAddItemDialog from './withAddItemDialog'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'
import getCroppedImg from './cropImage'

import DishEdit from './DishEdit'

function readFile(file) {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => resolve(reader.result), false)
    reader.readAsDataURL(file)
  })
}

export default compose(
  connect(
    ({ dishStore, categoryStore }) => {
      const { dish, noImageSrc, error, loading } = dishStore
      const { categories } = categoryStore
      const dishCategory = !isEmpty(categories)
        ? categories.find((c) => c.title === 'Блюда')
        : null
      const dishCategories = dishCategory
        ? categories.filter((c) => c.rootId === dishCategory._id)
        : []

      return {
        dish,
        noImageSrc,
        categories: dishCategories,
        error,
        loading,
      }
    },
    { getOneDish, setDish, saveDish, deleteDish, getCategories, addCategory }
  ),
  withRouter,
  withState('isVisible', 'setVisible', false),
  withState('isVisibleDelete', 'setIsVisibleDelete', false),
  withState('item', 'setItem', ''),
  withState('imgSrc', 'setImgSrc', null),
  withState('croppedAreaPixels', 'setCroppedAreaPixels', null),
  withState('croppedImg', 'setCroppedImg', null),
  withState('crop', 'setCrop', { x: 0, y: 0 }),
  withState('zoom', 'setZoom', 1),
  withState('imgName', 'setImgName', ''),
  withState('extraPrices', 'setExtraPrices', []),
  withProps(({ dish }) => ({
    modalTitle: 'Новая категория',
    modalText: '',
    modalLabel: 'Категория',
    dialogText: dish.title,
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  })),
  withHandlers({
    onChange: ({ dish, setDish }) => (index) => (event) => {
      setDish({
        ...dish,
        [index]: event.target.value,
      })
    },
    onSave: ({
      dish,
      extraPrices,
      imgSrc,
      croppedImg,
      saveDish,
      history,
      basePath,
    }) => () => {
      saveDish(
        {
          ...dish,
          upload: croppedImg ? croppedImg : imgSrc,
          extraPrices,
        },
        () => history.push(basePath)
      )
    },
    onDelete: ({ setIsVisibleDelete }) => () => {
      setIsVisibleDelete(true)
    },
    handleDialog: ({ setVisible }) => () => setVisible(true),
    cancelDialog: ({ setVisible, setItem }) => () => {
      setVisible(false)
      setItem('')
    },
    confirmDialog: ({ item, setVisible, setItem, addCategory }) => () => {
      addCategory({ title: item })
      setVisible(false)
      setItem('')
    },
    handleFileChange: ({ setImgSrc, setImgName }) => async (e) => {
      if (e.target.files && e.target.files.length > 0) {
        setImgName(e.target.files[0].name)
        const imageDataUrl = await readFile(e.target.files[0])
        setImgSrc(imageDataUrl)
      }
    },
    onCropComplete: ({ setCroppedAreaPixels }) => (
      croppedArea,
      croppedAreaPixels
    ) => {
      // console.log(croppedArea, croppedAreaPixels)
      setCroppedAreaPixels(croppedAreaPixels)
    },
    handleCropImage: ({
      imgSrc,
      setImgSrc,
      croppedAreaPixels,
      setCroppedImg,
    }) => async () => {
      const croppedImage = await getCroppedImg(imgSrc, croppedAreaPixels)
      setCroppedImg(croppedImage)
      setImgSrc(null)
    },
    dialogCancel: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(false),
    dialogConfirm: ({ dish, deleteDish, basePath, history }) => () => {
      deleteDish(dish._id, () => history.push(basePath))
    },
    onChangeExtraPrices: ({ extraPrices, setExtraPrices }) => (row) => (
      event
    ) => {
      setExtraPrices([
        ...extraPrices.slice(0, row),
        {
          ...extraPrices[row],
          [event.target.name]: event.target.value,
        },
        ...extraPrices.slice(row + 1),
      ])
    },
    handleExtraPrices: ({ extraPrices, setExtraPrices }) => () => {
      setExtraPrices([...extraPrices, {}])
    },
    handleDeleteExtraPrices: ({ extraPrices, setExtraPrices }) => (
      row
    ) => () => {
      setExtraPrices([
        ...extraPrices.slice(0, row),
        ...extraPrices.slice(row + 1),
      ])
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneDish(id)
      }
      this.props.getCategories()
    },
    componentDidUpdate(prevProps, prevState) {
      if (
        isEmpty(prevProps.dish) &&
        !isEmpty(this.props.dish) &&
        !isEmpty(this.props.dish.extraPrices)
      ) {
        this.props.setExtraPrices(this.props.dish.extraPrices)
      }
    },

    componentWillUnmount() {
      this.props.setDish({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  }),
  withAddItemDialog
)(DishEdit)
