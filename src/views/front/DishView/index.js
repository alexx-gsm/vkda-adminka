import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const DishList = lazy(() => import('./DishList'))
const DishEdit = lazy(() => import('./DishEdit'))

const basePath = '/front/dishes'

const DishView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <DishList basePath={basePath} theme={theme} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <DishEdit basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default DishView
