import { withRouter } from 'react-router-dom'
import { compose, withProps, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import socketIOClient from 'socket.io-client'
import { HOST } from '../../../../config'
// AC
import { getDishes, setDishes } from '../../../../redux/modules/dish'
import {
  getCategories,
  setSelectedCategory,
} from '../../../../redux/modules/category'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// tools
import isEmpty from '../../../../helpers/is-empty'

import DishList from './DishList'

const socket = socketIOClient(HOST)

export default compose(
  connect(
    ({ dishStore, categoryStore }) => {
      const { dishes, loading, error } = dishStore
      const { categories, selectedCategory } = categoryStore

      const filteredDishes = isEmpty(dishes)
        ? []
        : selectedCategory === ''
        ? dishes
        : dishes.filter((d) => d.categoryId === selectedCategory)

      const dishCategory = !isEmpty(categories)
        ? categories.find((c) => c.title === 'Блюда')
        : null
      const dishCategories = dishCategory
        ? categories.filter((c) => c.rootId === dishCategory._id)
        : []

      return {
        dishes: filteredDishes,
        categories: dishCategories,
        selectedCategory,
        loading,
        error,
      }
    },
    { getDishes, setDishes, getCategories, setSelectedCategory }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
    handleFilter: ({ setSelectedCategory }) => (e) => {
      setSelectedCategory(e.target.value)
    },
  }),
  lifecycle({
    componentDidMount() {
      // this.props.getDishes()
      this.props.getCategories()

      socket.emit('initial_dishes')
      socket.on('get_dishes', (dishes) => this.props.setDishes(dishes))
    },
    componentWillUnmount() {
      socket.off('get_dishes')
    },
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(DishList)
