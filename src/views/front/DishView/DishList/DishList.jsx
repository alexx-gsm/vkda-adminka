import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
// @material-ui components
import { Grid, Typography, Fab, TablePagination } from '@material-ui/core'
import { List, ListItem, IconButton } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-icons
import IconNavBefore from '@material-ui/icons/NavigateBefore'
import IconNavNext from '@material-ui/icons/NavigateNext'
import AddIcon from '@material-ui/icons/Add'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const DishList = ({
  dishes,
  categories,
  selectedCategory,
  handleFilter,
  basePath,
  formatter,
  loading,
  handleClick,
  error,
  classes,
  theme,
}) => {
  const [rowsPerPage, handleChangeRowsPerPage] = useState(5)
  const [page, handleChangePage] = useState(0)

  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <Grid container>
            <Grid item xs sm={4}>
              {categories && (
                <TextField
                  select
                  margin='none'
                  id='category'
                  value={isEmpty(selectedCategory) ? '-' : selectedCategory}
                  onChange={handleFilter}
                  className={classes.Select}
                  InputProps={{
                    classes: {
                      input: classes.SelectInput,
                      underline: classes.xsUnderlineWhite,
                    },
                  }}
                  SelectProps={{
                    classes: {
                      icon: classes.xsColorWhite,
                    },
                  }}
                  error={Boolean(error.category)}
                  helperText={error.category ? error.category : null}
                  fullWidth
                >
                  <MenuItem key={0} value='-'>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        Все
                      </Typography>
                    </Grid>
                  </MenuItem>
                  {categories.map((category) => {
                    return (
                      <MenuItem key={category._id} value={category._id}>
                        <Grid
                          container
                          alignItems='baseline'
                          className={classes.gridType}
                          direction='row'
                          wrap='wrap'
                        >
                          <Typography
                            className={classes.typoSelectInputTitle}
                            variant='h6'
                          >
                            {category.title}
                          </Typography>
                        </Grid>
                      </MenuItem>
                    )
                  })}
                </TextField>
              )}
            </Grid>
          </Grid>

          {!isEmpty(categories) && (
            <Fragment>
              <List className={classes.PaperDishes}>
                {dishes
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((cd) => (
                    <ListItem
                      key={cd._id}
                      button
                      divider
                      onClick={handleClick(cd._id)}
                      classes={{
                        root: classes._ListItem,
                      }}
                    >
                      <Grid
                        container
                        spacing={1}
                        justify='space-between'
                        alignItems='center'
                        wrap='nowrap'
                      >
                        <Grid container spacing={1} alignItems='baseline'>
                          <Grid item>
                            <Typography
                              variant='h6'
                              align='left'
                              className={classes._ListTitle}
                            >
                              {cd.title}
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography
                              variant='caption'
                              align='center'
                              className={classes.ListSubTitle}
                            >
                              /
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography
                              variant='caption'
                              align='center'
                              className={classes.ListSubTitle}
                            >
                              {
                                categories.find((c) => c._id === cd.categoryId)
                                  .title
                              }
                            </Typography>
                          </Grid>
                        </Grid>
                        <Grid item>
                          <Typography variant='h6' align='right'>
                            {cd.price ? formatter.format(cd.price) : ''}
                          </Typography>
                        </Grid>
                      </Grid>
                    </ListItem>
                  ))}
              </List>
            </Fragment>
          )}

          {/* PAGER */}
          <Grid className={classes._Row}>
            <Grid
              className={classes._GridActions}
              container
              spacing={2}
              justify='space-between'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item>
                <Fab
                  aria-label='Add'
                  color='secondary'
                  size='medium'
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              <Grid item>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={dishes.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={(e, page) => handleChangePage(page)}
                  onChangeRowsPerPage={(e) =>
                    handleChangeRowsPerPage(e.target.value)
                  }
                  labelRowsPerPage='Кол-во на стр.'
                />
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant='h6' className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
    </MuiThemeProvider>
  )
}

DishList.defaultProps = {
  dishes: [],
}

DishList.propTypes = {
  dishes: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  classes: PropTypes.object,
}

export default DishList
