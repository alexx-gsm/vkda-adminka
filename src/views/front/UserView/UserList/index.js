import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { getUsers } from '../../../../redux/modules/user'
// styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'

import UserList from './UserList'

export default compose(
  connect(
    ({ userStore, pagerStore }) => {
      const { users, loading } = userStore
      const { currentPage, itemsPerPage } = pagerStore
      return {
        users,
        currentPage,
        itemsPerPage,
        loading,
      }
    },
    { getUsers }
  ),
  withRouter,
  withHandlers({
    handleClick: ({ basePath, history }) => (id) => () =>
      history.push(`${basePath}/edit/${id}`),
  }),
  lifecycle({
    componentDidMount() {
      this.props.getUsers()
    },
    componentWillUnmount() {},
  }),
  withStyles(
    (theme) => ({
      ...CommonStyles(theme),
      ...styles(theme),
    }),
    { withTheme: true }
  )
)(UserList)
