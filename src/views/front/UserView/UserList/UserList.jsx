import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
// @material-ui components
import { Grid, Typography, Button, Fab } from '@material-ui/core'
import { List, ListItem, IconButton } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-icons
import IconNavBefore from '@material-ui/icons/NavigateBefore'
import IconNavNext from '@material-ui/icons/NavigateNext'
// @material-icons
import AddIcon from '@material-ui/icons/Add'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const UserList = ({
  users,
  handleClick,
  currentPage,
  maxPageNumber,
  handleNextPage,
  handlePrevPage,
  loading,
  basePath,
  theme,
  classes,
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading && !isEmpty(users) ? (
        <Fragment>
          <List className={classes.PaperList}>
            {users.map(user => (
              <ListItem
                key={user._id}
                button
                divider
                onClick={handleClick(user._id)}
                classes={{
                  root: classes._ListItem,
                }}
              >
                <Grid
                  container
                  spacing={1}
                  justify="space-between"
                  alignItems="center"
                  wrap="nowrap"
                >
                  <Grid container spacing={1} alignItems="baseline">
                    <Grid item>
                      <Typography variant="h6">{user.name}</Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </ListItem>
            ))}
          </List>

          {/* PAGER */}
          <Grid className={classNames(classes._Row, classes._PB)}>
            <Grid
              className={classes.MT20}
              container
              spacing={1}
              alignItems="center"
              wrap="nowrap"
            >
              <Grid item container>
                <Fab
                  aria-label="Add"
                  color="secondary"
                  size="medium"
                  to={`${basePath}/edit`}
                  component={Link}
                >
                  <AddIcon />
                </Fab>
              </Grid>
              {maxPageNumber > 1 && (
                <Fragment>
                  <Grid
                    item
                    container
                    justify="flex-end"
                    alignItems="baseline"
                    spacing={1}
                  >
                    <Grid item>
                      <Typography variant="h5">{currentPage + 1}</Typography>
                    </Grid>
                    <Grid item>
                      <Typography variant="h6">/</Typography>
                    </Grid>
                    <Grid item>
                      <Typography variant="h6">{maxPageNumber}</Typography>
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    container
                    wrap="nowrap"
                    style={{ width: 'auto' }}
                    alignItems="center"
                  >
                    <Grid item>
                      <IconButton
                        aria-label="Prev"
                        className={classes.margin}
                        disabled={currentPage === 0}
                        onClick={handlePrevPage}
                      >
                        <IconNavBefore fontSize="large" />
                      </IconButton>
                    </Grid>
                    <Grid item>
                      <IconButton
                        aria-label="Next"
                        className={classes.margin}
                        onClick={handleNextPage}
                        disabled={currentPage === maxPageNumber - 1}
                      >
                        <IconNavNext fontSize="large" />
                      </IconButton>
                    </Grid>
                  </Grid>
                </Fragment>
              )}
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <React.Fragment>
          <Typography variant="h6" className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </React.Fragment>
      )}
    </MuiThemeProvider>
  )
}

UserList.defaultProps = {
  customers: [],
}

export default UserList
