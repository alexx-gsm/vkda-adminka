import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const UserList = lazy(() => import('./UserList'))
const UserEdit = lazy(() => import('./UserEdit'))

const basePath = '/front/users'

const UserView = () => {
  return (
    <Suspense fallback={<LinearIndeterminate />}>
      <Switch>
        <Route
          exact
          path={`${basePath}`}
          render={() => <UserList basePath={basePath} theme={theme} />}
        />
        <Route
          path={`${basePath}/edit/:id?`}
          render={() => <UserEdit basePath={basePath} theme={theme} />}
        />
      </Switch>
    </Suspense>
  )
}

export default UserView
