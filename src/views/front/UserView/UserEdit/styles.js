export default (theme) => ({
  Wrap: {
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(1),
    },
  },
  PaperInfo: {
    padding: '0 20px 20px',
    marginTop: '20px',
    backgroundColor: theme.palette.grey[200],
  },
})
