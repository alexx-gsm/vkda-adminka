import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
// components
import AlertDialog from '../../../../components/AlertDialog'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// useful utils
import isEmpty from '../../../../helpers/is-empty'
import classNames from 'classnames'
// @material-ui
import { Paper, Grid, Typography, TextField, Fab } from '@material-ui/core'
import { Button, IconButton } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// @material-ui/icons
import IconDelete from '@material-ui/icons/Delete'

const UserEdit = ({
  user,
  basePath,
  classes,
  error,
  loading,
  onChange,
  onSave,
  onDelete,
  isVisibleDelete,
  dialogCancel,
  dialogConfirm,
  theme,
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Grid className={classes.Wrap}>
          <TextField
            required
            fullWidth
            id="name"
            label="ФИО"
            type="name"
            value={!isEmpty(user.name) ? user.name : ''}
            onChange={onChange('name')}
            margin="normal"
            error={Boolean(error.name)}
            helperText={error.name ? error.name : null}
          />

          <TextField
            fullWidth
            id="company"
            label="Организация"
            value={isEmpty(user.company) ? '' : user.company}
            onChange={onChange('company')}
            margin="normal"
            className={classes.wrapTitle}
            error={Boolean(error.company)}
            helperText={error.company ? error.company : null}
          />

          <Paper square className={classes.PaperInfo}>
            <TextField
              required
              fullWidth
              id="phone"
              label="Телефон"
              type="phone"
              value={!isEmpty(user.phone) ? user.phone : ''}
              onChange={onChange('phone')}
              className={classes.select}
              margin="normal"
              error={Boolean(error.phone)}
              helperText={error.phone ? error.phone : null}
            />
            <TextField
              required
              fullWidth
              id="address"
              label="Адрес доставки"
              type="address"
              value={!isEmpty(user.address) ? user.address : ''}
              onChange={onChange('address')}
              className={classes.select}
              margin="normal"
              error={Boolean(error.address)}
              helperText={error.address ? error.address : null}
            />
            <TextField
              fullWidth
              id="map"
              label="2gis"
              value={!isEmpty(user.map) ? user.map : ''}
              onChange={onChange('map')}
              className={classes.select}
              margin="normal"
            />
          </Paper>

          <TextField
            fullWidth
            id="note"
            label="Заметки"
            multiline
            rowsMax="5"
            value={!isEmpty(user.comment) ? user.comment : ''}
            onChange={onChange('comment')}
            margin="normal"
          />

          <Grid className={classNames(classes._MT2)}>
            <Grid
              container
              className={classes._GridActions}
              spacing={1}
              justify="space-between"
            >
              <Grid item>
                <Fab
                  size="small"
                  aria-label="Delete"
                  color="secondary"
                  onClick={onDelete}
                >
                  <IconDelete />
                </Fab>
              </Grid>
              <Grid item className={classes._MLAuto}>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label="Back"
                  variant="text"
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label="Save"
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      ) : (
        <Fragment>
          <Typography variant="h6" className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}

      <AlertDialog
        title="Удалить?"
        text={user.name}
        isVisible={isVisibleDelete}
        onCancel={dialogCancel}
        onConfirm={dialogConfirm}
      />
    </MuiThemeProvider>
  )
}

UserEdit.defaultProps = {
  customer: {},
  error: {},
}

export default UserEdit
