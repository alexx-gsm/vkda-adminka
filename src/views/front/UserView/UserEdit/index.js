import { connect } from 'react-redux'
import { compose, lifecycle, withHandlers, withState } from 'recompose'
import { withRouter } from 'react-router-dom'
// AC
import {
  setUser,
  saveUser,
  getOneUser,
  deleteUser,
} from '../../../../redux/modules/user'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// core components
import UserEdit from './UserEdit'
import isEmpty from '../../../../helpers/is-empty'

export default compose(
  connect(
    ({ userStore }) => {
      const { user, error, loading } = userStore
      return { user, error, loading }
    },
    { setUser, saveUser, getOneUser, deleteUser }
  ),
  withRouter,
  withState('isVisibleDelete', 'setIsVisibleDelete', false),
  withHandlers({
    onChange: ({ user, setUser }) => (name) => (event) => {
      setUser({
        ...user,
        [name]: event.target.value,
      })
    },
    onSave: ({ user, basePath, history, saveUser }) => () =>
      saveUser(user, () => history.push(basePath)),
    onDelete: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(true),
    dialogCancel: ({ setIsVisibleDelete }) => () => setIsVisibleDelete(false),
    dialogConfirm: ({
      user,
      deleteUser,
      setIsVisibleDelete,
      basePath,
      history,
    }) => () => {
      deleteUser(user._id)
      setIsVisibleDelete(false)
      history.push(basePath)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getOneUser(id)
      }
    },
    componentWillUnmount() {
      this.props.setUser({})
    },
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true,
  })
)(UserEdit)
