import axios from 'axios'
import jwt_decode from 'jwt-decode'
import { Record } from 'immutable'
import { setAuthToken } from '../../helpers/authHelpers'
import isEmpty from '../../helpers/is-empty'
import { appName, HOST } from '../../config'

//! MODULE NAME
export const moduleName = 'userStore'

//! ACTIONS
export const USER_LOADING = `${appName}/${moduleName}/LOADING`
export const USER_AUTH_SET = `${appName}/${moduleName}/AUTH_SET_ONE`
export const USER_ONE = `${appName}/${moduleName}/ONE`
export const USER_ALL = `${appName}/${moduleName}/ALL`
export const USER_ERROR = `${appName}/${moduleName}/ERROR`

//! INITIAL STATE
const ReducerRecord = Record({
  isAuthenticated: false,
  authUser: {},
  user: {},
  users: [],
  error: {},
  loading: false,
})

//! REDUCER
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case USER_LOADING:
      return state.set('loading', true)
    case USER_AUTH_SET:
      return state
        .set('loading', false)
        .set('isAuthenticated', !isEmpty(payload))
        .set('authUser', payload.authUser)
        .set('error', {})
    case USER_ONE:
      return state
        .set('loading', false)
        .set('user', payload.user)
        .set('error', {})
    case USER_ALL:
      return state
        .set('loading', false)
        .set('users', payload.users)
        .set('error', {})
    case USER_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 ** @desc   LOGIN USER
 *? @param  userData: User Object
 *? @param  cb: callback function
 */
export const login = (userData, cb) => (dispatch) => {
  axios
    .post(`${HOST}/api/users/login`, userData)
    .then((res) => {
      const { token } = res.data

      localStorage.setItem('jwtToken', token)
      setAuthToken(token)

      dispatch({
        type: USER_AUTH_SET,
        payload: { authUser: jwt_decode(token) },
      })

      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: USER_ERROR,
        error,
      })
    )
}

/**
 ** @desc   LOGOUT USER
 *? @param  cb: callback function
 */
export const logout = (cb) => (dispatch) => {
  localStorage.removeItem('jwtToken')
  setAuthToken(false)
  dispatch({
    type: USER_AUTH_SET,
    payload: { authUser: {} },
  })
  if (typeof cb === 'function') cb()
}

/**
 ** @desc   SET USER
 */
export const setUser = (user) => ({
  type: USER_ONE,
  payload: { user },
})

/**
 * * @desc  SAVE USER
 * ? @param {Object} user
 */
export const saveUser = (user, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/users`, user)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: USER_ERROR,
        error,
      })
    )
}

/**
 * * @desc  GET USERS
 */
export const getUsers = () => (dispatch) => {
  dispatch({
    type: USER_LOADING,
  })
  axios
    .post(`${HOST}/api/users/all`)
    .then((res) => {
      dispatch({
        type: USER_ALL,
        payload: { users: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: USER_ERROR,
        error,
      })
    )
}

/**
 ** @desc   GET USER BY ID
 */
export const getOneUser = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/users/${id}`)
    .then((res) => {
      dispatch({
        type: USER_ONE,
        payload: { user: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: USER_ERROR,
        error,
      })
    )
}

/**
 ** @desc   DELETE USER
 */
export const deleteUser = (id, cb) => (dispatch) => {
  axios
    .delete(`${HOST}/api/users/${id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: USER_ERROR,
        error,
      })
    )
}

export default reducer
