import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'
import isEmpty from '../../helpers/is-empty'

//! --- MODULE NAME ---
export const moduleName = 'categoryStore'

// --- ACTIONS ---
export const CATEGORY_LOADING = `${appName}/${moduleName}/LOADING`
export const CATEGORY_ALL = `${appName}/${moduleName}/GET_ALL`
export const CATEGORY_ONE = `${appName}/${moduleName}/GET_ONE`
export const CATEGORY_SET_FILTER = `${appName}/${moduleName}/SET_FILTER`
export const CATEGORY_ERROR = `${appName}/${moduleName}/ERROR`

export const CATEGORY_SET = `${appName}/${moduleName}/CATEGORY_SET`
export const CATEGORY_ADD = `${appName}/${moduleName}/CATEGORY_ADD`
export const CATEGORY_SAVE = `${appName}/${moduleName}/CATEGORY_SAVE`
export const CATEGORY_DELETE = `${appName}/${moduleName}/CATEGORY_DELETE`
export const CATEGORY_SET_SELECTED_ONE = `${appName}/${moduleName}/CATEGORY_SET_SELECTED_ONE`

//! --- INITIAL STATE ---
const emptyItem = {
  title: '',
  isRoot: false,
  rootId: null,
}

const ReducerRecord = Record({
  categories: [],
  category: emptyItem,
  selectedCategory: '',
  filter: null,
  error: {},
  loading: false,
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case CATEGORY_LOADING:
      return state.set('loading', true)
    case CATEGORY_ALL:
      return (
        state
          .set('loading', false)
          .set('categories', payload.categories)
          // .set('filter', !isEmpty(payload) ? payload[0]._id : null)
          .set('error', {})
      )
    case CATEGORY_ONE:
      return state
        .set('loading', false)
        .set('category', payload.category)
        .set('error', {})
    case CATEGORY_ADD:
      return state
        .set('loading', false)
        .set('categories', [...state.categories, payload.category])
        .set('error', {})
    case CATEGORY_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)

    case CATEGORY_SET_FILTER:
      return state.set('filter', payload.filter)

    case CATEGORY_SAVE:
      return state
        .set('loading', false)
        .set('category', {})
        .set('error', {})
    case CATEGORY_SET:
      return state
        .set('loading', false)
        .set('category', payload.category)
        .set('error', {})
    case CATEGORY_DELETE:
      return state
        .set('loading', false)
        .set('categories', [
          ...state.categories.filter((item) => item._id !== payload._id),
        ])
        .set('error', {})
    case CATEGORY_SET_SELECTED_ONE:
      return state.set('selectedCategory', payload.category)

    default:
      return state
  }
}

//! --- AC ---
/**
 **  @desc    GET ALL CATEGORIES
 */
export const getCategories = () => (dispatch) => {
  dispatch({
    type: CATEGORY_LOADING,
  })
  axios
    .post(`${HOST}/api/categories/all`)
    .then((res) => {
      dispatch({
        type: CATEGORY_ALL,
        payload: { categories: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_ERROR,
        error,
      })
    )
}

/**
 ** @desc    GET CATEGORY BY ID
 *? @param   {id} category id
 */
export const getOneCategory = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/categories/${id}`)
    .then((res) => {
      dispatch({
        type: CATEGORY_ONE,
        payload: { category: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_ERROR,
        error,
      })
    )
}

/**
 ** @desc  SET CATEGORY
 *? @param {object} category
 */
export const setCategory = (category) => ({
  type: CATEGORY_ONE,
  payload: { category },
})

/**
 ** @desc   ADD CATEGORY
 *? @param  {object} category
 */
export const addCategory = (category) => (dispatch) => {
  axios
    .post(`${HOST}/api/categories`, category)
    .then((res) => {
      dispatch({
        type: CATEGORY_ADD,
        payload: { category: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_ERROR,
        error,
      })
    )
}

/**
 ** @desc   SAVE CATEGORY
 *? @param  {object} category
 */
export const saveCategory = (category, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/categories`, category)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: CATEGORY_ERROR,
        error,
      })
    )
}

/**
 ** @desc   DELETE CATEGORY
 *? @param  {string} id of category
 *? @param  {function} callback
 */
export const deleteCategory = (id, cb) => (dispatch) => {
  if (id === '') {
    // history.push('/categories')
    return
  }
  axios
    .delete(`${HOST}/api/categories/${id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) => {
      dispatch({
        type: CATEGORY_ERROR,
        error,
      })
    })
}

/**
 ** @desc   SET CATEGORY FILTER
 *? @param  {filter} category id
 */
export const setCategoryFilter = (filter) => ({
  type: CATEGORY_SET_FILTER,
  payload: { filter },
})

// set selected category
export const setSelectedCategory = (category) => ({
  type: CATEGORY_SET_SELECTED_ONE,
  payload: { category },
})

export default reducer
