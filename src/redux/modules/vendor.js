import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'

// --- MODULE NAME ---
export const moduleName = 'vendorStore'

// --- ACTIONS ---
export const VENDOR_LOADING = `${appName}/${moduleName}/VENDOR_LOADING`
export const VENDOR_ALL = `${appName}/${moduleName}/VENDOR_ALL`
export const VENDOR_ONE = `${appName}/${moduleName}/VENDOR_ONE`
export const VENDOR_ERROR = `${appName}/${moduleName}/VENDOR_ERROR`

// --- INITIAL STATE ---
const emptyItem = {
  title: '',
}

const ReducerRecord = Record({
  vendors: [],
  vendor: emptyItem,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case VENDOR_LOADING:
      return state.set('loading', true)
    case VENDOR_ALL:
      return state
        .set('loading', false)
        .set('vendors', payload.vendors)
        .set('error', {})
    case VENDOR_ONE:
      return state
        .set('loading', false)
        .set('vendor', payload.vendor)
        .set('error', {})
    case VENDOR_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * * @desc    GET ALL VENDORS
 */
export const getAllVendors = () => (dispatch) => {
  dispatch({
    type: VENDOR_LOADING,
  })
  axios
    .post(`${HOST}/api/vendors/all`)
    .then((res) => {
      dispatch({
        type: VENDOR_ALL,
        payload: { vendors: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: VENDOR_ERROR,
        error,
      })
    )
}

/**
 * * @desc   GET ONE VENDOR
 * ? @param  id: vendor's id
 */
export const getOneVendor = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/vendors/${id}`)
    .then((res) => {
      dispatch({
        type: VENDOR_ONE,
        payload: { vendor: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: VENDOR_ERROR,
        error,
      })
    )
}

/**
 * * @desc    SET VENDOR
 */
export const setVendor = (vendor) => ({
  type: VENDOR_ONE,
  payload: { vendor },
})

/**
 * * @desc    SAVE VENDOR
 */
export const saveVendor = (vendor, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/vendors`, vendor)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: VENDOR_ERROR,
        error,
      })
    )
}

/**
 * * @desc    DELETE VENDOR
 */
export const deleteVendor = (id, cb) => (dispatch) => {
  if (!id) return

  axios
    .delete(`${HOST}/api/vendors/${id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) => {
      dispatch({
        type: VENDOR_ERROR,
        error,
      })
    })
}

export default reducer
