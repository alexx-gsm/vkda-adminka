import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'

// --- MODULE NAME ---
export const moduleName = 'productStore'

// --- ACTIONS ---
export const PRODUCT_LOADING = `${appName}/${moduleName}/PRODUCT_LOADING`
export const PRODUCT_ALL = `${appName}/${moduleName}/PRODUCT_ALL`
export const PRODUCT_ONE = `${appName}/${moduleName}/PRODUCT_ONE`
export const PRODUCT_ERROR = `${appName}/${moduleName}/PRODUCT_ERROR`

// --- INITIAL STATE ---
const emptyItem = {
  title: '',
  uom: ['уп', 'шт', 'кг', 'г', 'л', 'мл'],
  category: '',
  type: '',
  nutritionValue: {},
  tmap: '',
  writeOff: [],
}

const ReducerRecord = Record({
  products: [],
  product: emptyItem,
  type: ['Продукт', 'Заготовка'],
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case PRODUCT_LOADING:
      return state.set('loading', true)
    case PRODUCT_ALL:
      return state
        .set('loading', false)
        .set('products', payload.products)
        .set('error', {})
    case PRODUCT_ONE:
      return state
        .set('loading', false)
        .set('product', payload.product)
        .set('error', {})
    case PRODUCT_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * * @desc    GET ALL PRODUCTS
 */
export const getAllProducts = () => (dispatch) => {
  dispatch({
    type: PRODUCT_LOADING,
  })
  axios
    .post(`${HOST}/api/products/all`)
    .then((res) => {
      dispatch({
        type: PRODUCT_ALL,
        payload: { products: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: PRODUCT_ERROR,
        error,
      })
    )
}

/**
 * * @desc   GET ONE PRODUCT
 * ? @param  id: product's id
 */
export const getOneProduct = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/products/${id}`)
    .then((res) => {
      dispatch({
        type: PRODUCT_ONE,
        payload: { product: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: PRODUCT_ERROR,
        error,
      })
    )
}

/**
 * * @desc    SET PRODUCT
 */
export const setProduct = (product) => ({
  type: PRODUCT_ONE,
  payload: { product },
})

/**
 * * @desc    SAVE PRODUCT
 */
export const saveProduct = (product, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/products`, product)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: PRODUCT_ERROR,
        error,
      })
    )
}

/**
 * * @desc    DELETE PRODUCT
 */
export const deleteProduct = (id, cb) => (dispatch) => {
  if (!id) return

  axios
    .delete(`${HOST}/api/products/${id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) => {
      dispatch({
        type: PRODUCT_ERROR,
        error,
      })
    })
}

export default reducer
