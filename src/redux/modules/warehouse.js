import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'

// --- MODULE NAME ---
export const moduleName = 'warehouseStore'

// --- ACTIONS ---
export const WAREHOUSE_LOADING = `${appName}/${moduleName}/WAREHOUSE_LOADING`
export const WAREHOUSE_ALL = `${appName}/${moduleName}/WAREHOUSE_ALL`
export const WAREHOUSE_ONE = `${appName}/${moduleName}/WAREHOUSE_ONE`
export const WAREHOUSE_ERROR = `${appName}/${moduleName}/WAREHOUSE_ERROR`

// --- INITIAL STATE ---
const emptyItem = {
  title: '',
}

const ReducerRecord = Record({
  warehouses: [],
  warehouse: emptyItem,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case WAREHOUSE_LOADING:
      return state.set('loading', true)
    case WAREHOUSE_ALL:
      return state
        .set('loading', false)
        .set('warehouses', payload.warehouses)
        .set('error', {})
    case WAREHOUSE_ONE:
      return state
        .set('loading', false)
        .set('warehouse', payload.warehouse)
        .set('error', {})
    case WAREHOUSE_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * * @desc    GET ALL WAREHOUSES
 */
export const getAllWarehouses = () => (dispatch) => {
  dispatch({
    type: WAREHOUSE_LOADING,
  })
  axios
    .post(`${HOST}/api/warehouses/all`)
    .then((res) => {
      dispatch({
        type: WAREHOUSE_ALL,
        payload: { warehouses: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: WAREHOUSE_ERROR,
        error,
      })
    )
}

/**
 * * @desc   GET ONE WAREHOUSE
 * ? @param  id: warehouse's id
 */
export const getOneWarehouse = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/warehouses/${id}`)
    .then((res) => {
      dispatch({
        type: WAREHOUSE_ONE,
        payload: { warehouse: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: WAREHOUSE_ERROR,
        error,
      })
    )
}

/**
 * * @desc    SET WAREHOUSE
 */
export const setWarehouse = (warehouse) => ({
  type: WAREHOUSE_ONE,
  payload: { warehouse },
})

/**
 * * @desc    SAVE WAREHOUSE
 */
export const saveWarehouse = (warehouse, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/warehouses`, warehouse)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: WAREHOUSE_ERROR,
        error,
      })
    )
}

/**
 * * @desc    DELETE WAREHOUSE
 */
export const deleteWarehouse = (id, cb) => (dispatch) => {
  if (!id) return

  axios
    .delete(`${HOST}/api/warehouses/${id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) => {
      dispatch({
        type: WAREHOUSE_ERROR,
        error,
      })
    })
}

export default reducer
