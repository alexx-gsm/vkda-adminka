import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'
import isEmpty from '../../helpers/is-empty'
import moment from 'moment'
import 'moment/locale/ru'

//! --- MODULE NAME ---
export const moduleName = 'menuStore'

//! --- ACTIONS ---
export const MENU_LOADING = `${appName}/${moduleName}/LOADING`
export const MENU_ALL = `${appName}/${moduleName}/GET_ALL`
export const MENU_ONE = `${appName}/${moduleName}/GET_ONE`
export const MENU_TODAY = `${appName}/${moduleName}/GET_TODAY`
export const MENU_ERROR = `${appName}/${moduleName}/ERROR`
export const MENU_SET_TAB = `${appName}/${moduleName}/MENU_SET_TAB`

//! --- INITIAL STATE ---
const emptyItem = {
  date: moment().format('L'),
}

const ReducerRecord = Record({
  menus: [],
  menu: emptyItem,
  todayMenu: {},
  tomorrowMenu: {},
  tab: 0,
  error: {},
  loading: false,
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case MENU_LOADING:
      return state.set('loading', true)
    case MENU_ALL:
      return state
        .set('loading', false)
        .set('menus', payload.menus)
        .set('error', {})
    case MENU_ONE:
      return state
        .set('loading', false)
        .set('menu', !isEmpty(payload.menu) ? payload.menu : emptyItem)
        .set('error', {})
    case MENU_TODAY:
      return state
        .set('loading', false)
        .set('todayMenu', payload.todayMenu ? payload.todayMenu : {})
        .set('tomorrowMenu', payload.tomorrowMenu ? payload.tomorrowMenu : {})
        .set('error', {})
    case MENU_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)

    case MENU_SET_TAB:
      return state.set('tab', payload.tab)
    default:
      return state
  }
}

//! --- AC ---
/**
 * * @desc    GET ALL MENUS
 */
export const getMenus = () => (dispatch) => {
  dispatch({
    type: MENU_LOADING,
  })
  axios
    .post(`${HOST}/api/menus/all`)
    .then((res) => {
      dispatch({
        type: MENU_ALL,
        payload: { menus: res.data },
      })
    })
    .catch((error) => {
      dispatch({
        type: MENU_ERROR,
        error,
      })
    })
}

/**
 * * @desc    GET ONE MENU
 * ? @param   {_id}: menu._id
 */
export const getMenu = (_id) => (dispatch) => {
  axios
    .post(`${HOST}/api/menus/${_id}`)
    .then((res) => {
      dispatch({
        type: MENU_ONE,
        payload: { menu: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: MENU_ERROR,
        error,
      })
    )
}

/**
 * * @desc    GET TODAY MENU
 */
export const getTodayMenu = (today) => (dispatch) => {
  dispatch({
    type: MENU_LOADING,
  })
  axios
    .post(`${HOST}/api/menus/today`, { today })
    .then((res) => {
      dispatch({
        type: MENU_TODAY,
        payload: res.data,
      })
    })
    .catch((error) => {
      dispatch({
        type: MENU_ERROR,
        error,
      })
    })
}

/**
 * * @desc    SAVE MENU
 * ? @param   {menu}: menu instance
 * ? @param   {cb}: callback function
 */
export const saveMenu = (menu, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/menus`, menu)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) => {
      dispatch({
        type: MENU_ERROR,
        error,
      })
    })
}

/**
 * * @desc    SET MENU
 * ? @param   {menu}: menu instance
 */
export const setMenu = (menu) => ({
  type: MENU_ONE,
  payload: { menu },
})

/**
 * * @desc    SET TAB
 * ? @param   {tab}: weekly menus tab
 */
export const setTab = (tab) => ({
  type: MENU_SET_TAB,
  payload: { tab },
})

/**
 * * @desc    DELETE MENU
 * ? @param   {menu}: menu instance
 * ? @param   {cb}: callback function
 */
export const deleteMenu = (_id, cb) => (dispatch) => {
  axios
    .delete(`${HOST}/api/menus/${_id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) => {
      dispatch({
        type: MENU_ERROR,
        error,
      })
    })
}

export default reducer
