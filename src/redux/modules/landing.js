import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'

// --- MODULE NAME ---
export const moduleName = 'landingStore'

// --- ACTIONS ---
export const LANDING_LOADING = `${appName}/${moduleName}/LOADING`
export const LANDING_ALL = `${appName}/${moduleName}/GET_ALL`
export const LANDING_ONE = `${appName}/${moduleName}/GET_ONE`
export const LANDING_ERROR = `${appName}/${moduleName}/ERROR`

// --- INITIAL STATE ---
const emptyItem = {}

const ReducerRecord = Record({
  landings: [],
  landing: emptyItem,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case LANDING_LOADING:
      return state.set('loading', true)
    case LANDING_ALL:
      return state
        .set('loading', false)
        .set('landings', payload.landings)
        .set('error', {})
    case LANDING_ONE:
      return state
        .set('loading', false)
        .set('landing', payload.landing)
        .set('error', {})

    case LANDING_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * *    @desc   GET ALL LANDINGES
 */
export const getLandings = () => (dispatch) => {
  dispatch({
    type: LANDING_LOADING,
  })
  axios
    .post(`${HOST}/api/landings/all`)
    .then((res) => {
      dispatch({
        type: LANDING_ALL,
        payload: { landings: res.data },
      })
    })
    .catch((error) => {
      dispatch({
        type: LANDING_ERROR,
        error,
      })
    })
}

/**
 **   @desc   GET ONE LANDING
 *?   @param  id: landing._id
 */
export const getOneLanding = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/landings/${id}`)
    .then((res) => {
      dispatch({
        type: LANDING_ONE,
        payload: { landing: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: LANDING_ERROR,
        error,
      })
    )
}

/**
 **   @desc   SET LANDING
 *?   @param  landing: landing properties
 */
export const setLanding = (landing) => ({
  type: LANDING_ONE,
  payload: { landing },
})

/**
 **   @desc   SAVE LANDING
 *?   @param  landing: landing object
 *?   @param  cb: callback
 */
export const saveLanding = (landing, cb) => (dispatch) => {
  axios
    .put(`${HOST}/api/landings`, landing)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: LANDING_ERROR,
        error,
      })
    )
}

/**
 **   @desc   DELETE LANDING
 *?   @param  _id: landing._id
 *?   @param  cb: callback function
 */
export const deleteLanding = (_id, cb) => (dispatch) => {
  axios
    .delete(`${HOST}/api/landings/${_id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: LANDING_ERROR,
        error,
      })
    )
}

export default reducer
