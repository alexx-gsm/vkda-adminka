import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'
import moment from 'moment'
import 'moment/locale/ru'

// --- MODULE NAME ---
export const moduleName = 'orderStore'

// --- ACTIONS ---
export const ORDER_LOADING = `${appName}/${moduleName}/LOADING`
export const ORDER_ALL = `${appName}/${moduleName}/GET_ALL`
export const ORDER_ONE = `${appName}/${moduleName}/GET_ONE`
export const ORDER_SET_DELIVERY_DATE = `${appName}/${moduleName}/SET_DELIVERY_DATE`
export const ORDER_ERROR = `${appName}/${moduleName}/ERROR`

// --- INITIAL STATE ---
const emptyItem = {}

const ReducerRecord = Record({
  orders: [],
  order: emptyItem,
  selectedDeliveryDate: moment().format('L'),
  tabIndex: 0,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case ORDER_LOADING:
      return state.set('loading', true)
    case ORDER_ALL:
      return state
        .set('loading', false)
        .set('orders', payload.orders)
        .set('error', {})
    case ORDER_ONE:
      return state
        .set('loading', false)
        .set('order', payload.order)
        .set('error', {})
    case ORDER_SET_DELIVERY_DATE:
      return state
        .set('loading', false)
        .set('selectedDeliveryDate', moment(payload.date).format('L'))
        .set('error', {})
    case ORDER_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * *    @desc   GET ALL ORDERES
 */
export const getOrders = () => (dispatch) => {
  dispatch({
    type: ORDER_LOADING,
  })
  axios
    .post(`${HOST}/api/orders/all`)
    .then((res) => {
      dispatch({
        type: ORDER_ALL,
        payload: { orders: res.data },
      })
    })
    .catch((error) => {
      dispatch({
        type: ORDER_ERROR,
        error,
      })
    })
}

/**
 * *    @desc   GET DELIVERY ORDERES
 * ?    @param  {string} delivery_date
 */
export const getOrdersByDeliveryDate = (delivery_date) => (dispatch) => {
  dispatch({
    type: ORDER_LOADING,
  })
  axios
    .post(`${HOST}/api/orders/delivery`, { delivery_date })
    .then((res) => {
      dispatch({
        type: ORDER_ALL,
        payload: { orders: res.data },
      })
    })
    .catch((error) => {
      dispatch({
        type: ORDER_ERROR,
        error,
      })
    })
}

/**
 **   @desc   SET DELIVERY DATE
 *?   @param  {Date}: delivery date
 */
export const setDeliveryDate = (date) => ({
  type: ORDER_SET_DELIVERY_DATE,
  payload: { date },
})

/**
 **   @desc   GET ONE ORDER
 *?   @param  id: order._id
 */
export const getOneOrder = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/orders/${id}`)
    .then((res) => {
      dispatch({
        type: ORDER_ONE,
        payload: { order: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: ORDER_ERROR,
        error,
      })
    )
}

/**
 **   @desc   SET ORDER
 *?   @param  order: order properties
 */
export const setOrder = (order) => ({
  type: ORDER_ONE,
  payload: { order },
})

/**
 **   @desc   SAVE ORDER
 *?   @param  id: order._id
 */
export const saveOrder = (order, cb) => (dispatch) => {
  if (order.upload) {
    const formData = new FormData()
    formData.append('upload', order.upload)
    Object.keys(order).map((key) => formData.append(key, order[key]))
    axios
      .put(`${HOST}/api/orders`, formData)
      .then(() => {
        if (typeof cb === 'function') cb()
      })
      .catch((error) =>
        dispatch({
          type: ORDER_ERROR,
          error,
        })
      )
  } else {
    axios
      .put(`${HOST}/api/orders`, order)
      .then(() => {
        if (typeof cb === 'function') cb()
      })
      .catch((error) =>
        dispatch({
          type: ORDER_ERROR,
          error,
        })
      )
  }
}

/**
 **   @desc   DELETE ORDER
 *?   @param  _id: order._id
 *?   @param  cb: callback function
 */
export const deleteOrder = (_id, cb) => (dispatch) => {
  axios
    .delete(`${HOST}/api/orders/${_id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: ORDER_ERROR,
        error,
      })
    )
}

export default reducer
