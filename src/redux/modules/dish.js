import axios from 'axios'
import { appName, HOST } from '../../config'
import { Record } from 'immutable'

// --- MODULE NAME ---
export const moduleName = 'dishStore'

// --- ACTIONS ---
export const DISH_LOADING = `${appName}/${moduleName}/DISH_LOADING`
export const DISH_ALL = `${appName}/${moduleName}/DISH_ALL`
export const DISH_ONE = `${appName}/${moduleName}/DISH_ONE`
export const DISH_ERROR = `${appName}/${moduleName}/DISH_ERROR`

// --- INITIAL STATE ---
const emptyItem = {}

const ReducerRecord = Record({
  dishes: [],
  dish: emptyItem,
  noImageSrc: '/images/noimage.jpg', // no-image.jpeg
  tabIndex: 0,
  error: {},
  loading: false,
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case DISH_LOADING:
      return state.set('loading', true)
    case DISH_ALL:
      return state
        .set('loading', false)
        .set('dishes', payload.dishes)
        .set('error', {})
    case DISH_ONE:
      return state
        .set('loading', false)
        .set('dish', payload.dish)
        .set('error', {})
    case DISH_ERROR:
      return state
        .set('loading', false)
        .set('error', error.response ? error.response.data : error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * *    @desc   GET ALL DISHES
 */
export const getDishes = () => (dispatch) => {
  dispatch({
    type: DISH_LOADING,
  })
  axios
    .post(`${HOST}/api/dishes/all`)
    .then((res) => {
      dispatch({
        type: DISH_ALL,
        payload: { dishes: res.data },
      })
    })
    .catch((error) => {
      dispatch({
        type: DISH_ERROR,
        error,
      })
    })
}

/**
 **   @desc   GET ONE DISH
 *?   @param  id: dish._id
 */
export const getOneDish = (id) => (dispatch) => {
  axios
    .post(`${HOST}/api/dishes/${id}`)
    .then((res) => {
      dispatch({
        type: DISH_ONE,
        payload: { dish: res.data },
      })
    })
    .catch((error) =>
      dispatch({
        type: DISH_ERROR,
        error,
      })
    )
}

/**
 **   @desc   SET DISHES
 *?   @param  dishes
 */
export const setDishes = (dishes) => ({
  type: DISH_ALL,
  payload: { dishes },
})

/**
 **   @desc   SET DISH
 *?   @param  dish: dish properties
 */
export const setDish = (dish) => ({
  type: DISH_ONE,
  payload: { dish },
})

/**
 **   @desc   SAVE DISH
 *?   @param  id: dish._id
 */
export const saveDish = (dish, cb) => (dispatch) => {
  if (dish.upload) {
    const formData = new FormData()
    formData.append('upload', dish.upload)
    Object.keys(dish).map((key) => formData.append(key, dish[key]))
    axios
      .put(`${HOST}/api/dishes`, formData)
      .then(() => {
        if (typeof cb === 'function') cb()
      })
      .catch((error) =>
        dispatch({
          type: DISH_ERROR,
          error,
        })
      )
  } else {
    axios
      .put(`${HOST}/api/dishes`, dish)
      .then(() => {
        if (typeof cb === 'function') cb()
      })
      .catch((error) =>
        dispatch({
          type: DISH_ERROR,
          error,
        })
      )
  }
}

/**
 **   @desc   DELETE DISH
 *?   @param  _id: dish._id
 *?   @param  cb: callback function
 */
export const deleteDish = (_id, cb) => (dispatch) => {
  axios
    .delete(`${HOST}/api/dishes/${_id}`)
    .then(() => {
      if (typeof cb === 'function') cb()
    })
    .catch((error) =>
      dispatch({
        type: DISH_ERROR,
        error,
      })
    )
}

export default reducer
