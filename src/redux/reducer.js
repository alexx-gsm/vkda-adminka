import { combineReducers } from 'redux'
import sidebarReducer, { moduleName as sidebarModule } from './modules/sidebar'
import userReducer, { moduleName as userModule } from './modules/user'
import warehouseReducer, {
  moduleName as warehouseModule,
} from './modules/warehouse'
import vendorReducer, { moduleName as vendorModule } from './modules/vendor'
import productReducer, { moduleName as productModule } from './modules/product'
import dishReducer, { moduleName as dishModule } from './modules/dish'
import categoryReducer, {
  moduleName as categoryModule,
} from './modules/category'
import menuReducer, { moduleName as menuModule } from './modules/menu'
import orderReducer, { moduleName as orderModule } from './modules/order'
import landingReducer, { moduleName as landingModule } from './modules/landing'

export default combineReducers({
  [sidebarModule]: sidebarReducer,
  [userModule]: userReducer,
  [warehouseModule]: warehouseReducer,
  [vendorModule]: vendorReducer,
  [productModule]: productReducer,
  [dishModule]: dishReducer,
  [categoryModule]: categoryReducer,
  [menuModule]: menuReducer,
  [orderModule]: orderReducer,
  [landingModule]: landingReducer,
})
