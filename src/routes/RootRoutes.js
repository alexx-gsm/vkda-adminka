// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { u1F356 } from 'react-icons-kit/noto_emoji_regular/u1F356'
import { truck } from 'react-icons-kit/fa/truck'
import { rub } from 'react-icons-kit/fa/rub'
import { tags } from 'react-icons-kit/fa/tags'
import { ic_people } from 'react-icons-kit/md/ic_people'
import { fileTextO } from 'react-icons-kit/fa/fileTextO'
import { u1F35B } from 'react-icons-kit/noto_emoji_regular/u1F35B'
import { cutlery } from 'react-icons-kit/fa/cutlery'
import { database } from 'react-icons-kit/fa/database'
// views
import DashboardView from '../views/DashboardView'
// subroutes
import DashboardRoutes from './subroutes/DashboardRoutes'
import FrontRoutes from './subroutes/FrontRoutes'
import DirectoryRoutes from './subroutes/DirectoryRoutes'

const RootRoutes = [
  {
    path: '/',
    exact: true,
    invisible: true,
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: ic_dashboard,
    component: DashboardView,
    sidebar: DashboardRoutes,
  },
  {
    path: '/front',
    sidebarName: 'Фронт',
    navbarName: 'Front',
    icon: cutlery,
    component: FrontRoutes,
    sidebar: FrontRoutes,
  },
  {
    path: '/directories',
    sidebarName: 'Справочники',
    navbarName: 'Directories',
    icon: database,
    component: DirectoryRoutes,
    sidebar: DirectoryRoutes,
  },
  {
    invisible: true,
    path: '/invoices',
    sidebarName: 'Накладные',
    navbarName: 'Invoices',
    icon: ic_dashboard,
    component: DashboardView,
    sidebar: DashboardRoutes,
  },
  {
    invisible: true,
    path: '/acts',
    sidebarName: 'Акты',
    navbarName: 'Acts',
    icon: ic_dashboard,
    component: DashboardView,
    sidebar: DashboardRoutes,
  },
]

export default RootRoutes
