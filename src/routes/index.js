import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// Layouts
import AdminLayout from '../layouts/Admin'
import Login from '../layouts/Login'
// Private Route (only for authorized users)
import AdminRoute from './AdminRoute'

const indexRoutes = [{ path: '/', component: AdminLayout }]

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/login' component={Login} />
          {indexRoutes.map((prop, key) => {
            return (
              <AdminRoute
                path={prop.path}
                component={prop.component}
                key={key}
              />
            )
          })}
        </Switch>
      </Router>
    )
  }
}

export default Routes
