import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

const AdminRoute = ({
  component: Component,
  isAuthenticated,
  authUser,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated === true && authUser.role === 'admin' ? (
        <Component {...props} />
      ) : (
        <Redirect to='/login' />
      )
    }
  />
)

export default connect(
  ({ userStore }) => {
    const { isAuthenticated, authUser } = userStore

    return {
      isAuthenticated,
      authUser,
    }
  },
  {}
)(AdminRoute)
