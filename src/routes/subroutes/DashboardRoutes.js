// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { u1F356 } from 'react-icons-kit/noto_emoji_regular/u1F356'
import { truck } from 'react-icons-kit/fa/truck'
import { rub } from 'react-icons-kit/fa/rub'
import { tags } from 'react-icons-kit/fa/tags'
import { ic_people } from 'react-icons-kit/md/ic_people'
import { fileTextO } from 'react-icons-kit/fa/fileTextO'
import { u1F35B } from 'react-icons-kit/noto_emoji_regular/u1F35B'
// views
import DashboardView from '../../views/DashboardView'

const basePath = '/'

const DashboardRoutes = [
  {
    path: `${basePath}`,
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: ic_dashboard,
    component: DashboardView,
  },
]

export default DashboardRoutes
