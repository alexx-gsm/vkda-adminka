// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { shoppingBasket } from 'react-icons-kit/fa/shoppingBasket'
import { library } from 'react-icons-kit/icomoon/library'
import { truck } from 'react-icons-kit/icomoon/truck'
import { tags } from 'react-icons-kit/fa/tags'
// views
import DashboardView from '../../views/DashboardView'
import ProductView from '../../views/directories/ProductView'
import CategoryView from '../../views/directories/CategoryView'
import WarehouseView from '../../views/directories/WarehouseView'
import VendorView from '../../views/directories/VendorView'

const basePath = '/directories'

const DirectoryRoutes = [
  {
    path: `${basePath}`,
    exact: true,
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: ic_dashboard,
    component: DashboardView,
  },
  {
    path: `${basePath}/products`,
    sidebarName: 'Продукты',
    navbarName: 'Products',
    icon: shoppingBasket,
    component: ProductView,
  },
  {
    path: `${basePath}/categories`,
    sidebarName: 'Категории',
    navbarName: 'Categories',
    icon: tags,
    component: CategoryView,
  },
  {
    path: `${basePath}/warehouses`,
    sidebarName: 'Склады',
    navbarName: 'Warehouses',
    icon: library,
    component: WarehouseView,
  },
  {
    path: `${basePath}/vendors`,
    sidebarName: 'Поставщики',
    navbarName: 'Vendors',
    icon: truck,
    component: VendorView,
  },
]

export default DirectoryRoutes
