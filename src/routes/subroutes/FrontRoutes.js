// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { u1F35B } from 'react-icons-kit/noto_emoji_regular/u1F35B'
import { rouble } from 'react-icons-kit/fa/rouble'
import { fileTextO } from 'react-icons-kit/fa/fileTextO'
import { ic_people } from 'react-icons-kit/md/ic_people'
import { ic_shopping_basket } from 'react-icons-kit/md/ic_shopping_basket'
import { ic_language } from 'react-icons-kit/md/ic_language'
// views
import DashboardView from '../../views/DashboardView'
import DishView from '../../views/front/DishView'
import MenuView from '../../views/front/MenuView'
import OrderView from '../../views/front/OrderView'
// import UserView from '../../views/front/UserView'
import LandingView from '../../views/front/LandingView'

const basePath = '/front'

const FrontRoutes = [
  {
    path: `${basePath}`,
    exact: true,
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: ic_dashboard,
    component: DashboardView,
  },
  {
    path: `${basePath}/dishes`,
    sidebarName: 'Блюда',
    navbarName: 'Dishes',
    icon: u1F35B,
    component: DishView,
  },
  {
    path: `${basePath}/menus`,
    sidebarName: 'Меню',
    navbarName: 'Menus',
    icon: fileTextO,
    component: MenuView,
  },
  {
    path: `${basePath}/landing`,
    sidebarName: 'Сайт',
    navbarName: 'Landing',
    icon: ic_language,
    component: LandingView,
  },
  {
    path: `${basePath}/orders`,
    sidebarName: 'Заказы',
    navbarName: 'Orders',
    icon: ic_shopping_basket,
    component: OrderView,
  },
  // {
  //   path: `${basePath}/users`,
  //   sidebarName: 'Клиенты',
  //   navbarName: 'Users',
  //   icon: ic_people,
  //   component: UserView,
  // },
]

export default FrontRoutes
