export default theme => ({
  overlayWrapper: {
    position: 'relative',
    color: '#333',
    background: 'rgba(0, 0, 0, 0.4)',
    zIndex: 10,

    [theme.breakpoints.down('xs')]: {
      position: 'fixed',
      top: '60px',
      left: 0,
      right: 0,
      bottom: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  },
  overlay: {
    position: 'absolute',
    zIndex: 1,
    background: theme.palette.common.white,
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.15)',
    // right: 0,
    // left: 'auto',
    paddingTop: '20px',
    [theme.breakpoints.down('xs')]: {
      position: 'static'
    }
  }
})
