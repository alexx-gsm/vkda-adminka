import React from 'react'
// moment utils
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment'
import moment from 'moment'
import 'moment/locale/ru'
// Material UI
import { Typography, TextField } from '@material-ui/core'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import './style.css'
// Day Picker
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'

const DayPickerOverlay = ({
  classes,
  classNames,
  selectedDay,
  children,
  ...props
}) => {
  return (
    <div className={classes.overlayWrapper} {...props}>
      <div className={classes.overlay}>
        <Typography variant='caption' align='center'>
          Выбери день
        </Typography>
        {children}
      </div>
    </div>
  )
}

const withDayInput = ({ ...props }) => {
  return class extends React.Component {
    render() {
      return (
        <TextField
          id='day-picker'
          margin={this.props.margin ? this.props.margin : 'normal'}
          fullWidth
          {...this.props}
          {...props}
        >
          {this.props.value}
        </TextField>
      )
    }
  }
}

const DayPicker = ({
  value,
  format,
  classes,
  onDayClick,
  InputLabelProps,
  InputProps,
  label,
  margin,
}) => {
  return (
    <DayPickerInput
      component={withDayInput({
        InputLabelProps,
        InputProps,
        label,
        margin,
      })}
      formatDate={formatDate}
      value={value ? moment(value, 'L').toDate() : moment().toDate()}
      parseDate={parseDate}
      format={format}
      dayPickerProps={{
        showWeekNumbers: true,
        showOutsideDays: true,
        firstDayOfWeek: 1,
        todayButton: 'Сегодня',
        locale: 'ru',
        localeUtils: MomentLocaleUtils,
        onDayClick: onDayClick,
        onTodayButtonClick: onDayClick,
      }}
      overlayComponent={(props) => DayPickerOverlay({ ...props, classes })}
    />
  )
}

export default withStyles(styles, { theme: true })(DayPicker)
