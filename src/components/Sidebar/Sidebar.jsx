import React, { Fragment } from 'react'
import { Grid, Drawer, Typography } from '@material-ui/core'
import { NavLink } from 'react-router-dom'
// material-ui components
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import Divider from '@material-ui/core/Divider'
// react-icon-kit
import { Icon } from 'react-icons-kit'
// styles
import { withStyles } from '@material-ui/core'
import CommonStyles from '../../styles/CommonStyles'
import styles from './styles'

function Sidebar({ sidebar, classes }) {
  const AdapterLink = React.forwardRef((props, ref) => (
    <NavLink innerRef={ref} {...props} />
  ))

  return (
    <List component='nav'>
      {sidebar.map((route, i) => {
        if (route.divider) return <Divider key={i} />
        if (!route.redirect) {
          return (
            <ListItem
              key={route.path}
              button
              component={AdapterLink}
              to={route.path}
              className={classes.listItem}
              exact={route.exact}
              classes={{
                root: classes.listItemRoot,
              }}
            >
              {route.icon && (
                <ListItemIcon classes={{ root: classes.listItemIconRoot }}>
                  <Icon size={32} icon={route.icon} />
                </ListItemIcon>
              )}
              <ListItemText
                primary={route.sidebarName}
                primaryTypographyProps={{
                  classes: { root: classes.listItemTextRoot },
                }}
              />
            </ListItem>
          )
        }
      })}
    </List>
  )
}

export default withStyles(
  (theme) => ({ ...CommonStyles(theme), ...styles(theme) }),
  {
    withTheme: true,
  }
)(Sidebar)
