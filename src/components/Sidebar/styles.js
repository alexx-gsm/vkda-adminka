export default theme => ({
  listItem: {
    '&.active$listItemRoot': {
      background: theme.palette.primary.main,
    },
    '&.active $listItemIconRoot, &.active $listItemTextRoot': {
      color: theme.palette.primary.contrastText,
    },
  },
  listItemRoot: {},
  listItemIconRoot: {
    margin: 0,
  },
  listItemTextRoot: {},
})
