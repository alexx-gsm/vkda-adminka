import { compose, withProps, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { logout } from '../../redux/modules/user'
// styles
import { withStyles } from '@material-ui/core/styles'
import CommonStyles from '../../styles/CommonStyles'
import styles from './style'
// tools
import isEmpty from '../../helpers/is-empty'

// component
import Appbar from './Appbar'

export default compose(
  connect(
    ({ userStore }) => {
      const { isAuthenticated, authUser } = userStore

      return {
        isAuthenticated,
        authUser,
      }
    },
    {
      logout,
    }
  ),
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withRouter,
  withHandlers({
    onLogout: ({ logout, history }) => () =>
      logout(() => history.push('/login')),
  }),
  lifecycle({
    componentDidMount() {},
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    withTheme: true,
  })
)(Appbar)
