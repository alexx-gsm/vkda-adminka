import React, { useState, Fragment } from 'react'
import { Link, NavLink, Switch, Route } from 'react-router-dom'
import { object } from 'prop-types'
// material-ui components
import { Grid, AppBar, Toolbar } from '@material-ui/core'
import { Hidden, Drawer } from '@material-ui/core'
import { Button, IconButton } from '@material-ui/core'
// @material-ui/icons
import MenuIcon from '@material-ui/icons/Menu'
// react-icon-kit
import { Icon } from 'react-icons-kit'

import RootRoutes from '../../routes/RootRoutes'
// components
import Sidebar from '../Sidebar'

function Appbar({ isAuthenticated, onLogout, classes }) {
  const [isOpen, handleOpen] = useState(false)

  const switchRoutes = (routes) => (
    <Switch>
      {routes.map((route) => {
        return (
          <Route
            path={route.path}
            render={() => (
              <Fragment>
                <Hidden mdUp>
                  <Drawer
                    variant='temporary'
                    anchor='left'
                    open={isOpen}
                    onClick={() => handleOpen(false)}
                    ModalProps={{
                      keepMounted: true, // Better open performance on mobile.
                    }}
                  >
                    <Sidebar sidebar={route.sidebar} />
                  </Drawer>
                </Hidden>
                <Hidden smDown>
                  <Drawer
                    variant='permanent'
                    open
                    classes={{
                      paper: classes.drawerPaper,
                    }}
                  >
                    <div className={classes._ToolBar} />

                    <Sidebar sidebar={route.sidebar} />

                    <Grid className={classes._MTAuto}>
                      <Button onClick={onLogout} fullWidth>
                        Выйти
                      </Button>
                    </Grid>
                  </Drawer>
                </Hidden>
              </Fragment>
            )}
            key={route.sidebarName}
            exact={route.exact}
          />
        )
      })}
    </Switch>
  )

  const AdapterLink = React.forwardRef((props, ref) => (
    <NavLink innerRef={ref} {...props} />
  ))

  return (
    <Fragment>
      <AppBar position='fixed'>
        <Toolbar className={classes.toolBar}>
          <IconButton
            color='inherit'
            aria-label='Open drawer'
            onClick={() => handleOpen(true)}
            className={classes.navIconHide}
          >
            <MenuIcon />
          </IconButton>
          <Button
            to={'/'}
            component={Link}
            aria-label='Home'
            variant='text'
            className={classes.ButtonLogo}
          >
            ВкусноДА!
          </Button>

          <Hidden xsDown>
            {RootRoutes.filter((r) => !r.invisible).map((route) => {
              return (
                <Button
                  to={route.path}
                  component={AdapterLink}
                  key={route.sidebarName}
                  variant='text'
                  className={classes.ButtonTopMenu}
                >
                  {route.sidebarName}
                </Button>
              )
            })}
          </Hidden>
          <Hidden smUp>
            {RootRoutes.filter((r) => !r.invisible).map((route) => {
              return (
                <IconButton
                  to={route.path}
                  component={AdapterLink}
                  key={route.sidebarName}
                  className={classes.IconTopMenu}
                >
                  <Icon size={32} icon={route.icon} />
                </IconButton>
              )
            })}
          </Hidden>
        </Toolbar>
      </AppBar>

      {switchRoutes(RootRoutes)}
    </Fragment>
  )
}

Appbar.propTypes = {
  classes: object.isRequired,
}

export default Appbar
