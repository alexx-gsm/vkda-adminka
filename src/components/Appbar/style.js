const drawerWidth = '200px'

export default (theme) => ({
  toolBar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  ButtonLogo: {
    color: theme.palette.common.white,
    marginRight: 'auto',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 'auto',
    },
  },
  ButtonTopMenu: {
    color: theme.palette.common.white,
    borderRadius: 0,
    borderBottom: `1px solid transparent`,
    '&.active': {
      borderBottom: `1px solid ${theme.palette.grey[400]}`,
    },
  },
  IconTopMenu: {
    color: theme.palette.common.white,
    '&.active': {
      color: theme.palette.grey[600],
    },
  },

  drawerPaper: {
    width: drawerWidth,
    position: 'static',
  },
})
