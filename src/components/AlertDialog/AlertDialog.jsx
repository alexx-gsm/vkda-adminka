import React from 'react'
// Material UI
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

const AlertDialog = ({
  title,
  text,
  isVisible,
  onCancel,
  onConfirm,
  classes
}) => {
  return (
    <div>
      <Dialog
        open={isVisible}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
        classes={{
          paper: classes.Dialog
        }}
      >
        <DialogTitle id='alert-dialog-title' className={classes.Title}>
          {title}
        </DialogTitle>
        <DialogContent className={classes.Content}>
          <DialogContentText id='alert-dialog-description'>
            {text}
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.Actions}>
          <Button
            color='secondary'
            size='small'
            onClick={() => {
              onConfirm()
              onCancel()
            }}
          >
            Удалить
          </Button>
          <Button
            variant='contained'
            onClick={onCancel}
            autoFocus
            color='primary'
          >
            Отмена
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default AlertDialog
