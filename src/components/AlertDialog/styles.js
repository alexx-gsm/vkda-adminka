export default (theme) => ({
  Dialog: {
    minWidth: '400px',
    [theme.breakpoints.down('xs')]: {
      minWidth: '330px',
    },
  },
  Title: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing(2),
  },
  Content: {
    padding: `${theme.spacing(3)}px ${theme.spacing(2)}px`,
  },
  Actions: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing(2),
  },
})
