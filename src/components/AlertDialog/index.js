// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

import AlertDialog from './AlertDialog'

export default withStyles(styles)(AlertDialog)
