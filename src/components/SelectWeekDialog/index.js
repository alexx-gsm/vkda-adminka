// Material UI
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

import SelectWeekDialog from './SelectWeekDialog'

export default withStyles(styles)(SelectWeekDialog)
