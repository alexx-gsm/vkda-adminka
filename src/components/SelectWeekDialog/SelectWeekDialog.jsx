import React, { useState } from 'react'
import DayPicker from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import './style.css'
import moment from 'moment'
import MomentLocaleUtils from 'react-day-picker/moment'
import 'moment/locale/ru'
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core'
import { DialogActions, Button, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'

function getWeekDays(weekStart) {
  const days = [weekStart]
  for (let i = 1; i < 7; i += 1) {
    days.push(
      moment(weekStart)
        .add(i, 'days')
        .toDate()
    )
  }
  return days
}

function getWeekRange(date) {
  return {
    from: moment(date)
      .startOf('week')
      .toDate(),
    to: moment(date)
      .endOf('week')
      .toDate(),
  }
}

function SelectWeekDialog({
  selectedDays,
  setSelectedDays,
  isOpen,
  handleClose,
  classes,
}) {
  const [hoverRange, setHoverRange] = useState(undefined)

  const handleDayChange = (date) =>
    setSelectedDays(getWeekDays(getWeekRange(date).from))

  const handleDayEnter = (date) => setHoverRange(getWeekRange(date))

  const handleDayLeave = () => setHoverRange(undefined)

  const handleWeekClick = (weekNumber, days, e) => setSelectedDays(days)

  const daysAreSelected = selectedDays.length > 0

  const modifiers = {
    hoverRange,
    selectedRange: daysAreSelected && {
      from: selectedDays[0],
      to: selectedDays[6],
    },
    hoverRangeStart: hoverRange && hoverRange.from,
    hoverRangeEnd: hoverRange && hoverRange.to,
    selectedRangeStart: daysAreSelected && selectedDays[0],
    selectedRangeEnd: daysAreSelected && selectedDays[6],
  }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='customized-dialog-title'
      open={isOpen}
      classes={{ paper: classes.DialogPaper }}
    >
      <DialogTitle
        id='customized-dialog-title'
        onClose={handleClose}
        classes={{ root: classes.DialogTitleRoot }}
      >
        Выбрать неделю
        <IconButton
          aria-label='Close'
          className={classes.closeButton}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: classes.DialogContentRoot }}>
        <DayPicker
          selectedDays={selectedDays}
          showWeekNumbers
          showOutsideDays
          todayButton={'Сегодня'}
          modifiers={modifiers}
          onDayClick={handleDayChange}
          onTodayButtonClick={handleDayChange}
          onDayMouseEnter={handleDayEnter}
          onDayMouseLeave={handleDayLeave}
          onWeekClick={handleWeekClick}
          localeUtils={MomentLocaleUtils}
          locale={'ru'}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => setSelectedDays([])}
          color='primary'
          disabled={selectedDays.length === 0}
        >
          Очистить
        </Button>
        <Button
          onClick={handleClose}
          color='secondary'
          variant='contained'
          disabled={selectedDays.length === 0}
        >
          Выбрать
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default SelectWeekDialog
