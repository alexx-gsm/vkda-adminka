export default (theme) => ({
  DialogPaper: {
    [theme.breakpoints.down('xs')]: {
      margin: theme.spacing(1),
    },
  },
  DialogTitleRoot: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  DialogContentRoot: {
    padding: theme.spacing(1),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.secondary.contrastText,
  },
})
