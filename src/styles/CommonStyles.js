import { HOST } from '../config'

export default (theme) => ({
  _Root: {
    flexGrow: 1,
    minHeight: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed',
  },
  _Main: {
    flexGrow: 1,
    overflow: 'auto',
    maxWidth: '100%',
    boxSizing: 'border-box',
    [theme.breakpoints.down('sm')]: {
      padding: 0,
    },
  },
  _ToolBar: theme.mixins.toolbar,

  _Wrap: {
    maxWidth: '1200px',
    width: '100%',
    margin: '0 auto',
    flex: 1,
    boxSizing: 'border-box',
    padding: theme.spacing(3),
    background: 'rgba(255, 255, 255, 0.2)',
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(2),
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
    },
  },
  _Row: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  _Row2: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  _GridActions: {
    marginTop: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      marginTop: 0,
      flexDirection: 'column-reverse',
    },
  },
  _PT2: {
    paddingTop: theme.spacing(2),
  },
  _PT3: {
    paddingTop: theme.spacing(3),
  },
  _PB: { paddingBottom: theme.spacing(1) },
  _PB2: { paddingBottom: theme.spacing(2) },
  _PL2: { paddingLeft: theme.spacing(2) },
  _P1: {
    padding: theme.spacing(1),
  },
  _P2: {
    padding: theme.spacing(2),
  },
  _P3: {
    padding: theme.spacing(3),
  },
  _PH2: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  _PV2: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  _ML2: { marginLeft: theme.spacing(2) },
  _MT2: { marginTop: theme.spacing(2) },
  _MT3: {
    marginTop: theme.spacing(3),
  },
  _MRAuto: {
    marginRight: 'auto',
  },
  _MLAuto: {
    marginLeft: 'auto',
    [theme.breakpoints.down('sm')]: {
      marginLeft: 0,
    },
  },
  _MTAuto: {
    marginTop: 'auto',
  },
  _ListTitle: {
    fontSize: '1.2rem',
    lineHeight: '1',
    [theme.breakpoints.down('xs')]: {
      fontSize: '1rem',
    },
  },
})
